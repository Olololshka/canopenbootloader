#include <cstring>

#include "LinkInfoStorage.h"

#include "CRC32_platform.h"

/*! \addtogroup Application
 * @{
 */

#ifndef CANOPEN_BOOTLOADER_INITIAL_BITRATE
#error "CANOPEN_BOOTLOADER_INITIAL_BITRATE not defined!"
#else
#if CANOPEN_BOOTLOADER_INITIAL_BITRATE != 10 &&                                \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 20 &&                                \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 50 &&                                \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 125 &&                               \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 250 &&                               \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 500 &&                               \
    CANOPEN_BOOTLOADER_INITIAL_BITRATE != 1000
#error "CANOPEN_BOOTLOADER_INITIAL_BITRATE not applicatable"
#endif
#endif

#define INITIAL_BITRATE                                                        \
  CANOPEN_BOOTLOADER_INITIAL_BITRATE ///< Cold-boot bitrate
#define LSS_NODE_ID_ASSIGNMENT 0xff  ///< Special NODE-ID for unconfigured nodes

/// Placeholder for memory to use as LinkInfo storage
/// not LinkInfo class object, because default constructor
/// will be called
static volatile char Linkinfo_placeholder[sizeof(LinkInfo)]
    __attribute__((section(".SharedStorage.LinkInfo")));

/// Manager to control access to LinkInfo storage
SharedStorageManager<LinkInfo, MemcpyStoragePolcy>
    LinkInfoStorage(&Linkinfo_placeholder);

void LinkInfo::resetDefaults() {
  node_ID = LSS_NODE_ID_ASSIGNMENT;
  bitrate = INITIAL_BITRATE;
  update_crc();
}

bool LinkInfo::checkConfigured() {
  static const uint16_t speeds[] = {10, 20, 50, 125};

  if (!check_crc())
    return false;

  if (node_ID < 1 || node_ID > 127) {
    return false;
  }

  for (size_t i = 0; i < sizeof(speeds) / sizeof(uint16_t); ++i) {
    if (speeds[i] == bitrate) {
      return true;
    }
  }
  return false;
}

void LinkInfo::update_crc() {
  crc32 = 0;
  auto newcrc32 =
      CRC32_platform().accamulate((uint8_t *)this, sizeof(LinkInfo));
  crc32 = newcrc32;
}

bool LinkInfo::check_crc() const {
  LinkInfo s(*this);
  s.update_crc();
  return s.crc32 == crc32;
}

/*! @} */
