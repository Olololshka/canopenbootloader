/*!
 * \file
 * \brief In memory CANopen bus settings storage
 *
 * This file defines special memory storage for CANopen NODE-ID and bit-rate
 * Workflow scenarios:
 * - Power up   -> Avoid initialize LinkInfo, it contains random data
 *              -> Bootloader startup
 *              -> check if LinkInfo valid - fail
 *              -> Use LSS to achieve correct NODE-ID and bit-rate
 *              -> write correct LinkInfo
 *              -> start application
 *              -> application reads and apply CAN settings from shared storage
 *
 * - Power up   -> Avoid initialize LinkInfo, it contains random data
 *              -> Bootloader startup
 *              -> check if LinkInfo valid - fail
 *              -> Use LSS to achieve correct NODE-ID and bit rate
 *              -> write correct LinkInfo
 *              -> receive NMI reboot request
 *              -> software reset
 *              -> Bootloader startup
 *              -> check if LinkInfo valid - success
 *              -> do not force LSS assign new NODE-ID
 *
 * - Power up   -> Avoid initialize LinkInfo, it contains random data
 *              -> Bootloader startup
 *              -> check if LinkInfo valid - fail
 *              -> Use LSS to achieve correct NODE-ID and bit-rate
 *              -> write correct LinkInfo
 *              -> start application
 *              -> application reads and apply CAN settings from shared storage
 *              -> receive NMI reboot request
 *              -> software reset
 *              -> Bootloader startup
 *              -> check if LinkInfo valid - success
 *              -> do not force LSS assign new NODE-ID
 */

#ifndef _Bootloader_App_Shared_data_H_
#define _Bootloader_App_Shared_data_H_

#include <stdint.h>

#include "Shared_storage.h"

/*! \addtogroup Application
 * @{
 */

/*!
 * \brief Shared storage class for CANopen NODE-ID and bit-rate
 *
 * Use specific ram section .SharedStorage.LinkInfo to avoid save value between
 * reboots, uses crc32 to check itself integrity
 */
struct LinkInfo {
  /*!
   * \brief Constructor, reset class to unconfigured but valid state, update crc
   */
  LinkInfo() { resetDefaults(); }

  /*!
   * \brief Copy constructor
   */
  LinkInfo(const LinkInfo &) = default;

  /*!
   * \brief Reset NODE-ID to LSS_NODE_ID_ASSIGNMENT and bitrate to
   * INITIAL_BITRATE, update crc to correct value
   */
  void resetDefaults();

  /*!
   * \brief Verify configuration
   * \return true, if bitrate is one of avalable values NODE-ID !=
   * LSS_NODE_ID_ASSIGNMENT and < 127, crc is correct
   */
  bool checkConfigured();

  /*!
   * \brief Update crc field after change NODE-ID or bitrate
   */
  void update_crc();

  /*!
   * \brief Check current crc32
   * \return crc32 equals real crc32 of fields node_ID and bitrate
   */
  bool check_crc() const;

  uint8_t node_ID;  ///< CANopen NODE-ID
  uint16_t bitrate; ///< bitrate from list {10, 20, 50, 125} (for ISO11898-2)

private:
  uint32_t crc32; ///<  crc value
} __attribute__((packed));

extern SharedStorageManager<LinkInfo, MemcpyStoragePolcy> LinkInfoStorage;

#endif /* _Bootloader_App_Shared_data_H_ */
