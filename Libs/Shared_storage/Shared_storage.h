/*!
 * \file
 * \brief Bootloader and application shared storage
 */

#ifndef _SHARED_STORAGE_H_
#define _SHARED_STORAGE_H_

#include <new>
#include <stdint.h>
#include <stdlib.h>
#include <type_traits>

/*! \addtogroup Application
 * @{
 */

/*! \brief Base serialisator template, not correctly instanceable */
template <typename T, typename Enable = void> struct Serializator {};

/*!
 * \brief Template specialization to process C-compatible structures
 * \tparam T type of C-compatible struct to be processed this serialisator
 */
template <typename T>
struct Serializator<
    T, typename std::enable_if<std::is_copy_constructible<T>::value>::type> {
  /*!
   * \brief Get size of type T
   * \return sizeof(T)
   */
  static constexpr size_t getSize() { return sizeof(T); }

  /*!
   * \brief Serialise C-compatable struct by copy it to prevusly allicated dest
   * \param dest pointer to place where src will be serialised
   * \param src C-compatable struct what will be serialised
   */
  static void serialize(void *dest, const T &src) {
    /* https://stackoverflow.com/a/1554808/8065921
     * Copy-construct new T in dest */
    new (dest) T(src);
  }

  /*!
   * \brief Deserialise C-compatable struct by copy-construct it from data
   * \param data Pointer to prototype of T
   * \return Copy-constructed T from data
   */
  static T deserialize(void *data) { return T(*static_cast<T *>(data)); }
};

/*!
 * \brief Template for storage manager
 * \tparam T type of C-compatible struct to be processed this serializator
 * \tparam Tpolicy class what will be used to read write serialized data
 *
 * Tpolicy class must contain two methods load() and store():
 * \code
 *  struct example_polcy {
 *      bool store(const void *data, size_t size) const;
 *      void load(void *pos, size_t size) const;
 *  };
 * \endcode
 */
template <typename T, typename Tpolicy> struct SharedStorageManager {
  using serializator_t = Serializator<T>;

  /*!
   * \brief Store T by Tpolicy
   * \param obj Object to be stored
   * \return true, if success
   */
  bool store(const T &obj) const {
    constexpr auto size = serializator_t::getSize();
    uint8_t buf[size];

    serializator_t::serialize(buf, obj);
    return polcy.store(buf, size);
  }

  /*!
   * \brief Load T from storage by Tpolicy
   * \return instance of type T, contains data from storage
   */
  T load() const {
    constexpr auto size = serializator_t::getSize();
    uint8_t buf[size];

    polcy.load(buf, size);
    return serializator_t::deserialize(buf);
  }

  /*!
   * \brief Constructor
   *
   * Accept parameters to create on Tpolicy instance
   * \param Tpolicy_args will be passed to Tpolicy constructor
   */
  template <typename... Tpolicy_args>
  SharedStorageManager(const Tpolicy_args &...policy_args)
      : polcy(policy_args...) {}

private:
  Tpolicy polcy;
};

/*!
 * \brief Default memcpy()-based storage policy
 *
 * Use base address as flat memory storage
 */
class MemcpyStoragePolcy {
public:
  /*!
   * \brief Constructor
   * \param addr pointer to storage position
   */
  MemcpyStoragePolcy(volatile void *addr) : addr(addr) {}

  /*!
   * \brief Store data
   *
   * Copy size bytes from data to storage
   * \param data Pointer to data will be stored
   * \param size size of data
   * \return true if success
   */
  bool store(const void *data, size_t size) const;

  /*!
   * \brief Load data from storage
   *
   * Copy size bytes from start of storage to pos
   * \param pos pointer to allocated place what data will be coped to
   * \param size size of data to read
   */
  void load(void *pos, size_t size) const;

protected:
  volatile void *addr; ///< pointer to storage
};

/*! @} */

#endif /* _SHARED_STORAGE_H_ */
