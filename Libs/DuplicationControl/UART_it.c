#include "hw_includes.h"

#include "DuplicationUartIRQ.h"

#if RESERV_ENABLE
extern UART_HandleTypeDef duplicationChaninConnection;

void /*interrupt*/ _gen_uart_string(RESERV_UART_PORT)(void) {
  HAL_UART_IRQHandler(&duplicationChaninConnection);
}
#endif
