#ifndef DUPLICATIONCONTROL_H
#define DUPLICATIONCONTROL_H

#include <cstdarg>
#include <cstdint>
#include <cstdlib>

struct DuplicationControl {
  using command_cb = void (*)(void *data, size_t size);
  using action_cb = void (*)();

  static constexpr size_t MAX_TRANSFER_UNIT_SIZE = 1024;
  static constexpr uint8_t MAGICK = 0x84;

  //! Main to reserve kit message types
  enum MessageType {
    //! Periodical Main Kit Repeat
    PMKR = 0,
    //! Network Config Changed
    NCC = 1,
    //! OD updated
    ODU = 2,
  };

  //! Do not call this, use duplication global object instead
  DuplicationControl(bool reserv_active_lvl);

  //! Returns true, if this is reserv hw kit
  bool IsReservChannel() const;

  //! Reserved hw kit await start point
  void StartBarier(uint8_t *pOD, uint16_t odsize = 0, bool is_app = false);
  void StartBarier(bool is_app = false) { StartBarier(nullptr, 0, is_app); }

  //! Main kit informs reserve what can satae was changed
  void CANStateChanged(uint8_t state_code);

  //! For reserve kit: get master kit can state on startup
  uint8_t getTargetCanState() const;

  //! Call this, then master kit network config was changed
  void NetworkConfigChanged(void *data, uint16_t size) {
    sendBuffer(data, size, NCC);
  }
  template <typename T> void NetworkConfigChanged(T &data) {
    NetworkConfigChanged(reinterpret_cast<void *>(&data), sizeof(T));
  }

  //! Call this, is app settings saved
  void SettingsSaved() { sendBuffer(pOD, odsize, ODU); }

  //! Call this periodicaly in CAN thread to process reservation messages
  void pool();

  //! Call this before switching mode to close uart and pins
  void destroy();

  //! will be called from StartBarier() context if main kit updates it's network
  //! config
  command_cb onNetconfig;

  //! will be called from StartBarier() context if main kit saves it's OD
  command_cb onODSaved;

  //! request to:
  //! Bootloader: Start application
  //! Application: Go to bootloader
  action_cb onShitchWorkingMode;

  void (*diagnosticCb)(const char *fmt, va_list &args);

  action_cb time_waste;

private:
  bool awaitData(uint8_t *ptr, uint16_t size, uint32_t &global_timeout);
  bool processPMKR(uint32_t &global_timeout);
  bool processBufferRessive(uint32_t &global_timeout, command_cb callback);

  void sendBuffer(const void *data, const uint16_t size,
                  const MessageType type);

  uint32_t CalcODCrc() const;
  void await_TX_complead() const;

  void printDiagnostic(const char *fmt, ...) const;

  uint32_t last_pool_tick;
  uint8_t targetCanState;

  uint8_t *pOD;
  uint16_t odsize;

  bool reserv_active_lvl;
  bool is_app;
};

extern DuplicationControl duplication;

#endif // DUPLICATIONCONTROL_H
