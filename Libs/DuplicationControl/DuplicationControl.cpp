#include <cstring>
#include <limits>
#include <new>

#include "hw_includes.h"

#include "DuplicationControl.h"

#define RESERVATION_LOOP_MAGICK 0x84
// More - not works. Incorrect data on ressive side
#define RESERVATION_LOOP_BITRATE 256000

#define RESERVATION_LOOP_POOL_PERIOD_MIN 100
#define RESERVATION_LOOP_POOL_AWAIT_PERIOD                                     \
  (RESERVATION_LOOP_POOL_PERIOD_MIN * 20)

#define _gen_port_stringp(l) GPIO##l
#define _gen_port_string(l) _gen_port_stringp(l)

#define _gen_clk_enp(p) __HAL_RCC_GPIO##p##_CLK_ENABLE
#define _gen_clk_en(pv) _gen_clk_enp(pv)

#define _gen_clk_enpin(pin) (1 << pin)

#define _gen_uart_stringp(n) USART##n
#define _gen_uart_string(n) _gen_uart_stringp(n)

#define _gen_uart_irqnp(n) USART##n##_IRQn
#define _gen_uart_irqn(n) _gen_uart_irqnp(n)

#if RESERV_ENABLE
#if !defined(RESERV_DETECT_PORT) || !defined(RESERV_DETECT_PIN) ||             \
    RESERV_DETECT_PIN < 0 || !defined(RESERV_UART_PORT) ||                     \
    !defined(RESERV_DETECT_ACTIVE_LVL)
#error "Reservation definition incostistant!"
#endif
#endif

#if !defined(NDEBUG) || 1
#define DIAG(fmt, ...) printDiagnostic(fmt "\n", ##__VA_ARGS__)
#else
#define DIAG(...)
#endif

struct PocketBase {
  PocketBase() = default;
  PocketBase(uint8_t id) : MAGICK{RESERVATION_LOOP_MAGICK}, ID(id) {}

  uint8_t MAGICK;
  uint8_t ID;
} __PACKED;

struct PMKR : public PocketBase {
  PMKR() : PocketBase{DuplicationControl::PMKR}, _CANState{}, _IsApp{} {}
  PMKR(uint8_t CANState, bool IsApplication)
      : PocketBase{DuplicationControl::PMKR}, _CANState{CANState},
        _IsApp{IsApplication} {}

  uint8_t *data_ptr() { return &_CANState; }

  bool IsApplication() const { return !!_IsApp; }
  uint8_t CANState() const { return _CANState; }

private:
  uint8_t _CANState;
  uint8_t _IsApp;
} __PACKED;

DuplicationControl duplication(RESERV_DETECT_ACTIVE_LVL);

#if RESERV_ENABLE
UART_HandleTypeDef duplicationChaninConnection{
    _gen_uart_string(RESERV_UART_PORT),
    {
        .BaudRate = RESERVATION_LOOP_BITRATE,
        .WordLength = UART_WORDLENGTH_9B, // 8b + parity
        .StopBits = UART_STOPBITS_1,
        .Parity = UART_PARITY_EVEN,
        .Mode = UART_MODE_TX_RX,
        .HwFlowCtl = UART_HWCONTROL_NONE,
        .OverSampling = UART_OVERSAMPLING_16,
    }};

static uint8_t buff[DuplicationControl::MAX_TRANSFER_UNIT_SIZE];
#endif

static uint32_t time_diff(uint32_t start, uint32_t stop) {
  return start <= stop ? stop - start
                       : (std::numeric_limits<uint32_t>::max() - start) + stop;
}

DuplicationControl::DuplicationControl(bool reserv_active_lvl)
    : onNetconfig{}, onODSaved{}, onShitchWorkingMode{}, diagnosticCb{},
      time_waste{[]() { __WFI(); }}, last_pool_tick{},
      targetCanState{}, pOD{}, odsize{},
      reserv_active_lvl(reserv_active_lvl), is_app{false} {

#if RESERV_ENABLE
  _gen_clk_en(RESERV_DETECT_PORT)();
  GPIO_InitTypeDef GPIO_InitStruct{.Pin = _gen_clk_enpin(RESERV_DETECT_PIN),
                                   .Mode = GPIO_MODE_INPUT,
                                   .Pull = GPIO_PULLUP,
                                   .Speed = GPIO_SPEED_FREQ_LOW};

  HAL_GPIO_Init(_gen_port_string(RESERV_DETECT_PORT), &GPIO_InitStruct);
#endif
}

bool DuplicationControl::IsReservChannel() const {
#if !RESERV_ENABLE
  return false;
#else
  return HAL_GPIO_ReadPin(_gen_port_string(RESERV_DETECT_PORT),
                          _gen_clk_enpin(RESERV_DETECT_PIN)) ^
         !reserv_active_lvl;
#endif
}

void DuplicationControl::StartBarier(uint8_t *pOD, uint16_t odsize,
                                     bool is_app) {
  this->is_app = is_app;

  this->pOD = pOD;
  this->odsize = odsize;

  last_pool_tick = HAL_GetTick();

#if RESERV_ENABLE
  // do not init in constructor -unknown CPU Speed
  HAL_UART_Init(&duplicationChaninConnection);
  __HAL_UART_ENABLE_IT(&duplicationChaninConnection, UART_IT_RXNE);

  // main kit starts inmidienly
  if (!IsReservChannel()) {
    return;
  }

  uint32_t alive_timer = RESERVATION_LOOP_POOL_AWAIT_PERIOD;

  // await messages form master kit for timeout. If no messages ressived -
  // return
  while (true) {
    uint8_t b;
    bool result;

    // read magick
    result = awaitData(&b, 1, alive_timer);
    if (!result) {
      DIAG("Resv: MAGICK - timeout");
      return;
    }

    // if not magick - try ressive next
    if (b != RESERVATION_LOOP_MAGICK) {
      DIAG("Resv: MAGICK invalid: 0x%X", b);
      continue;
    }

    // read command code
    result = awaitData(&b, 1, alive_timer);
    if (!result) {
      DIAG("Resv: Command code - timeout");
      return;
    }

    switch (b) {
    case DuplicationControl::PMKR:
      if (processPMKR(alive_timer)) {
        DIAG("Resv: PMKR processed");
        alive_timer = RESERVATION_LOOP_POOL_AWAIT_PERIOD;
      } else {
        DIAG("Resv: PMKR processied with error");
      }
      break;
    case DuplicationControl::NCC:
      if (processBufferRessive(alive_timer, onNetconfig)) {
        DIAG("Resv: NCC processed");
        alive_timer = RESERVATION_LOOP_POOL_AWAIT_PERIOD;
      } else {
        DIAG("Resv: NCC processied with error");
      }
      break;
    case DuplicationControl::ODU:
      if (processBufferRessive(alive_timer, onODSaved)) {
        DIAG("Resv: ODU processed");
        alive_timer = RESERVATION_LOOP_POOL_AWAIT_PERIOD;

      } else {
        DIAG("Resv: ODU processied with error");
      }
      break;
    default: // unknown command code
      break;
    }
  }

#endif
}

void DuplicationControl::CANStateChanged(uint8_t state_code) {
  targetCanState = state_code;
}

uint8_t DuplicationControl::getTargetCanState() const { return targetCanState; }

void DuplicationControl::pool() {
#if RESERV_ENABLE
  auto ticks = HAL_GetTick();
  if (time_diff(last_pool_tick, ticks) >= RESERVATION_LOOP_POOL_PERIOD_MIN) {
    last_pool_tick = ticks;
    await_TX_complead();

    auto pmkr = new (buff) struct PMKR(targetCanState, is_app);
    HAL_UART_Transmit_IT(&duplicationChaninConnection,
                         reinterpret_cast<uint8_t *>(pmkr),
                         sizeof(struct PMKR));
  }
#endif
}

void DuplicationControl::destroy() {
#if RESERV_ENABLE
  await_TX_complead();

  __HAL_UART_DISABLE_IT(&duplicationChaninConnection, UART_IT_RXNE);
  HAL_UART_DeInit(&duplicationChaninConnection);

  HAL_GPIO_DeInit(_gen_port_string(RESERV_DETECT_PORT),
                  _gen_clk_enpin(RESERV_DETECT_PIN));
#endif
}

bool DuplicationControl::awaitData(uint8_t *ptr, uint16_t size,
                                   uint32_t &global_timeout) {
#if RESERV_ENABLE
  if (HAL_UART_Receive_IT(&duplicationChaninConnection, ptr, size) != HAL_OK) {
    return false;
  }

  auto start_waiting = HAL_GetTick();
  auto end_waiting = start_waiting + global_timeout;

  while (true) {
    time_waste();
    auto state = HAL_UART_GetState(&duplicationChaninConnection);
    auto now = HAL_GetTick();

    if ((state & HAL_UART_STATE_BUSY_RX) != HAL_UART_STATE_BUSY_RX) {
      global_timeout -= time_diff(start_waiting, now);
      return true;
    }

    if (start_waiting < end_waiting) {
      // normal
      if (now >= end_waiting) {
        break;
      }
    } else if (now >= end_waiting && now < start_waiting) {
      // overflow
      break;
    }
  }
  global_timeout = 0;

  HAL_UART_AbortReceive_IT(&duplicationChaninConnection);
#endif
  return false;
}

bool DuplicationControl::processPMKR(uint32_t &global_timeout) {
  struct PMKR res;

  if (awaitData(res.data_ptr(), sizeof(res) - sizeof(PocketBase),
                global_timeout)) {
    targetCanState = res.CANState();
    if ((res.IsApplication() != is_app) && (onShitchWorkingMode != nullptr)) {
      onShitchWorkingMode();
    }
    return true;
  } else {
    return false;
  }
}

bool DuplicationControl::processBufferRessive(uint32_t &global_timeout,
                                              command_cb callback) {
  uint16_t size;

  if (!awaitData(reinterpret_cast<uint8_t *>(&size), sizeof(size),
                 global_timeout)) {
    return false;
  }

  if ((size == 0) || (size > MAX_TRANSFER_UNIT_SIZE)) {
    return false; // size incorrect
  }

#if RESERV_ENABLE
  if (!awaitData(buff, size, global_timeout)) {
    return false;
  }

  if (callback != nullptr) {
    callback(buff, size);
  }
#endif

  return true;
}

void DuplicationControl::sendBuffer(const void *data, const uint16_t size,
                                    const MessageType type) {
#if RESERV_ENABLE
  if (size > MAX_TRANSFER_UNIT_SIZE - sizeof(size)) {
    return; // reject
  }

  await_TX_complead();

  std::memcpy(buff, &size, sizeof(size));
  std::memcpy(&buff[sizeof(size)], data, size);

  PocketBase base{static_cast<uint8_t>(type)};

  HAL_UART_Transmit(&duplicationChaninConnection,
                    reinterpret_cast<uint8_t *>(&base), sizeof(base), 10);
  HAL_UART_Transmit_IT(&duplicationChaninConnection, buff, size + sizeof(size));
#endif
}

void DuplicationControl::printDiagnostic(const char *fmt, ...) const {
  if (diagnosticCb) {
    va_list argptr;
    va_start(argptr, fmt);
    diagnosticCb(fmt, argptr);
    va_end(argptr);
  }
}

void DuplicationControl::await_TX_complead() const {
#if RESERV_ENABLE
  while ((HAL_UART_GetState(&duplicationChaninConnection) &
          HAL_UART_STATE_BUSY_TX) == HAL_UART_STATE_BUSY_TX) {
    time_waste();
  }
#endif
}
