#ifndef DUPLICATIONUARTIRQ_H
#define DUPLICATIONUARTIRQ_H

#if RESERV_ENABLE
#define _gen_uart_stringp(n) USART##n##_IRQHandler
#define _gen_uart_string(n) _gen_uart_stringp(n)
#else
#define _gen_uart_string(n) NULL
#endif

#endif // DUPLICATIONUARTIRQ_H
