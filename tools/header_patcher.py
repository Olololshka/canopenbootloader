#!/usr/bin/python

import os
import sys
from ast import literal_eval


def int2bytes(v):
    return v.to_bytes(4, byteorder='little')

if len(sys.argv) < 4:
    print("Usage: {} <version file> <hash file> <target header file>".format(sys.argv[0]))
    exit(-1)

with open(sys.argv[1], 'r') as f:
    version = int(f.read(), 16)

with open(sys.argv[2], 'r') as f:
    hash_data = int(f.read())

with open(sys.argv[3], 'r+b') as target:
    target.seek(5 * 4)
    target.write(int2bytes(version))
    target.write(int2bytes(hash_data))
