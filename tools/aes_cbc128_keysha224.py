from Crypto.Cipher import AES
from Crypto.Hash import SHA224
import hexdump

def getkey(header):
    h = SHA224.new()
    h.update(header)
    digestsha224 = h.digest()
    key = [None] * 16
    for i in range(14):
        key[i] = digestsha224[i * 2]

    key[14] = digestsha224[len(digestsha224) - 3]
    key[15] = digestsha224[len(digestsha224) - 1]
    return bytes(key)


def execute(infile, outfile):
    with open(infile, 'rb') as f:
        rawdata = f.read()

    header = rawdata[0:9*4]

    key = getkey(header)
    iv = rawdata[4:16 + 4]

    crypted_part_len = len(rawdata[9 * 4:])
    if crypted_part_len % AES.block_size != 0:
        to_add = AES.block_size - crypted_part_len % AES.block_size
        rawdata += b'\x00' * to_add

    cipher = AES.new(key, AES.MODE_CBC, iv)
    e_firmware = header + cipher.encrypt(rawdata[9 * 4:])

    with open(outfile, 'wb') as f:
        f.write(e_firmware)

    print("File {} encrypted by AES_CBC128.keySHA224 to {}\nkey =".format(infile, outfile))
    hexdump.hexdump(key)
    print("iv=")
    hexdump.hexdump(iv)
