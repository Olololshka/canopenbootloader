#!/usr/bin/bash

CURRENT_GIT_VERSION=$(git log -1 --format=%h)
VERSION_FILE=$1

if [[ -f $VERSION_FILE ]]; then
    PREV_VERSION=$(cat $VERSION_FILE)
else
    PREV_VERSION=""
fi

if [[ "$CURRENT_GIT_VERSION" != "$PREV_VERSION" ]]; then
    echo "Git version updated"
    echo $CURRENT_GIT_VERSION > $VERSION_FILE
fi
