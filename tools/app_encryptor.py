#!/usr/bin/python

import os
import sys

# Usage: app_encryptor.py encryption_algorithm source.bin destination.deploy

algorithms = ["aes_cbc128_keysha224"]

_forced_algorithm = 0

try:
    algorithm = _forced_algorithm if _forced_algorithm >= 0 else int(sys.argv[1]) - 1
    module_name = algorithms[algorithm]
    enc_module = __import__(module_name)
except Exception:
    print("Failed to load encryption algorithm '{}'".format(algorithms[algorithm]))
    exit(1)

args_start = 1 if _forced_algorithm >= 0 else 2

enc_module.execute(*sys.argv[args_start:])
