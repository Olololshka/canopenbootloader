find_package(PythonInterp REQUIRED)

FUNCTION(STM32_GET_FAMITY_X CHIP FAMILY_X)
    SET(re "^[sS][tT][mM]32(....).(.)")
    STRING(REGEX REPLACE ${re} "\\1" STM32_SERIES ${CHIP})
    STRING(REGEX REPLACE ${re} "\\2" STM32_SIZE_CODE ${CHIP})
    STRING(TOUPPER ${STM32_SERIES} STM32_SERIES)
    STRING(TOUPPER ${STM32_SIZE_CODE} STM32_SIZE_CODE)

    set(${FAMILY_X} STM32${STM32_SERIES}x${STM32_SIZE_CODE} PARENT_SCOPE)
    set(${FAMILY_X}_nosizecode STM32${STM32_SERIES}xx PARENT_SCOPE)
ENDFUNCTION()

MACRO(STM32_UPDATE_DEFAULTS)
        IF(NOT STM32_FLASH_ORIGIN)
        SET(STM32_FLASH_ORIGIN "0x08000000")
    ENDIF()

    IF(NOT STM32_RAM_ORIGIN)
        SET(STM32_RAM_ORIGIN "0x20000000")
    ENDIF()

    IF(NOT STM32_MIN_STACK_SIZE)
        SET(STM32_MIN_STACK_SIZE "0x200")
    ENDIF()

    IF(NOT STM32_MIN_HEAP_SIZE)
        SET(STM32_MIN_HEAP_SIZE "0")
    ENDIF()

    IF(NOT STM32_CCRAM_ORIGIN)
        SET(STM32_CCRAM_ORIGIN "0x10000000")
    ENDIF()

    IF(NOT STM32_CCRAM_SIZE)
        SET(STM32_CCRAM_SIZE "64K")
    ENDIF()
ENDMACRO()

FUNCTION(STM32_FILL_LD_TEMPLATE LD_SCRIPT_IN LD_SCRIPT_OUT)
    STM32_UPDATE_DEFAULTS()

    STM32_GET_CHIP_PARAMETERS(${STM32_CHIP} STM32_FLASH_SIZE STM32_RAM_SIZE)

    include(stm32_linker_F4)
    configure_file(${LD_SCRIPT_IN} ${LD_SCRIPT_OUT} @ONLY)
ENDFUNCTION()

FUNCTION(STM32_SET_TARGET_LD_SPEC TARGET SPEC)
    get_target_property(TARGET_LD_FLAGS ${TARGET} LINK_FLAGS)
    if (TARGET_LD_FLAGS)
        SET(TARGET_LD_FLAGS "${TARGET_LD_FLAGS} --specs=${SPEC}")
    else()
        SET(TARGET_LD_FLAGS "--specs=${SPEC}")
    endif()
    SET_TARGET_PROPERTIES(${TARGET} PROPERTIES LINK_FLAGS ${TARGET_LD_FLAGS})
ENDFUNCTION()

FUNCTION(STM32_SET_LD_SCRIPT TARGET LD_SCRIPT)
    GET_TARGET_PROPERTY(TARGET_LD_FLAGS ${TARGET} LINK_FLAGS)
    IF(TARGET_LD_FLAGS)
        SET(TARGET_LD_FLAGS "\"-T${LD_SCRIPT}\" ${TARGET_LD_FLAGS}")
    ELSE()
        SET(TARGET_LD_FLAGS "\"-T${LD_SCRIPT}\"")
    ENDIF()
    SET_TARGET_PROPERTIES(${TARGET} PROPERTIES LINK_FLAGS ${TARGET_LD_FLAGS})
ENDFUNCTION()

################################################################################

set(git_version_updater  ${CANOPEN_BOOLOADER_SCRIPTS_DIR}/git_version_updater.sh)

################################################################################

# делает копию всех секций приложения из .elf в .hashable.bin
# для последующего вычисления crc32
function(CALC_EXECUTABLE_HASH TARGET OUTPUT_HASH_FILE)
    add_custom_command(OUTPUT ${OUTPUT_HASH_FILE}
        COMMAND ${CMAKE_OBJCOPY} -O binary
            -j .text -j .rodata -j .preinit_array
            -j .ARM
            -j .ARM.extab # added in L4
            -j .init_array -j .fini_array -j .data -j .isr_vectors
            # no settings section
            $<TARGET_FILE:${TARGET}> $<TARGET_FILE:${TARGET}>.hashable.bin
        COMMAND ${PYTHON_EXECUTABLE} -c
            "import sys;import zlib;print(zlib.crc32(sys.stdin.buffer.read())%(1<<32))"
            < $<TARGET_FILE:${TARGET}>.hashable.bin
            > ${OUTPUT_HASH_FILE}
        DEPENDS ${TARGET}
        VERBATIM
    )
endfunction()

################################################################################

set(header_patcher ${CANOPEN_BOOLOADER_SCRIPTS_DIR}/header_patcher.py)

function(FILL_APPLICATION_HEADER OUTPUT_FILE INPUT_TARGET)
    set(git_version_file ${CMAKE_CURRENT_BINARY_DIR}/version.git)
    set(hashfile ${CMAKE_CURRENT_BINARY_DIR}/${INPUT_TARGET}.hash)
    set(header_file $<TARGET_FILE:${INPUT_TARGET}>.header.bin)
    CALC_EXECUTABLE_HASH(${INPUT_TARGET} ${hashfile})

    add_custom_command(OUTPUT ${OUTPUT_FILE}
        COMMAND
            ${git_version_updater} ${git_version_file}
        COMMAND
            ${CMAKE_OBJCOPY} -O binary -j .header $<TARGET_FILE:${INPUT_TARGET}> ${header_file}
        COMMAND
            ${PYTHON_EXECUTABLE} ${header_patcher} ${git_version_file} ${hashfile}
                ${header_file}
        COMMAND
            ${CMAKE_OBJCOPY} --update-section .header=${header_file}
                $<TARGET_FILE:${INPUT_TARGET}> ${OUTPUT_FILE}
        DEPENDS ${INPUT_TARGET} ${hashfile}
    )
    add_custom_target(${OUTPUT_FILE}.target DEPENDS ${OUTPUT_FILE})
endfunction()

################################################################################

set(app_encryptor ${CANOPEN_BOOLOADER_SCRIPTS_DIR}/app_encryptor.py)

function(ENCRYPT_APP_IMAGE INPUT_FILE APP_ENCRYPTED_IMAGE)
    set(APP_RAW_IMAGE ${INPUT_ELF}.raw.bin)

    add_custom_command(OUTPUT ${APP_ENCRYPTED_IMAGE}
        COMMAND
            ${CMAKE_OBJCOPY} -O binary ${INPUT_FILE} ${APP_RAW_IMAGE}
        COMMAND
            ${PYTHON_EXECUTABLE} ${app_encryptor} ${APP_RAW_IMAGE} ${APP_ENCRYPTED_IMAGE}
        DEPENDS ${INPUT_FILE}
    )
    add_custom_target(${APP_ENCRYPTED_IMAGE}.target ALL DEPENDS ${APP_ENCRYPTED_IMAGE})
endfunction()

################################################################################

set(git_version_updater  ${CANOPEN_BOOLOADER_SCRIPTS_DIR}/git_version_updater.sh)

# patch OD[1018sub3] to git version
function(EDS_1018sub3_git_version INPUT_EDS_FILE OUTPUT_EDS_FILE)
    set(git_version_file ${CMAKE_CURRENT_BINARY_DIR}/version.git)
    add_custom_command(OUTPUT ${git_version_file}
        COMMAND ${git_version_updater} ${git_version_file}
    )
    add_custom_command(OUTPUT ${OUTPUT_EDS_FILE}
        COMMAND
            ${PYTHON_EXECUTABLE} -c
                "import re,sys;f=open(sys.argv[1], 'r').read();repl=r'\\1=0x{}\\n'.format(input());print(re.sub('(\\[1018sub3\\][\\nA-Za-z0-9= ;]*DefaultValue)=0\\n', repl, f))"
                ${INPUT_EDS_FILE} < ${git_version_file} > ${OUTPUT_EDS_FILE}
        DEPENDS ${INPUT_EDS_FILE}
                ${git_version_file}
        VERBATIM
    )
endfunction()
