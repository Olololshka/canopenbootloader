#[=======================================================================[.rst:
GetMacroValue
-----------------

Provides a macro to get C macro value ``C``.

.. command:: get_macro_value

  ::

    get_macro_value(<macro> <file> <variable>)

  get value of the ``<symbol>`` after including given header ``<file>`` and
  store its value in a ``<variable>``.  Specify the list of files in one
  argument as a semicolon-separated list. ``<variable>`` will be created as an
  internal cache variable.

The following variables may be set before calling this macro to modify
the way the check is run:

``CMAKE_REQUIRED_DEFINITIONS``
  list of macros to define (-DFOO=bar)
``CMAKE_REQUIRED_INCLUDES``
  list of include directories
``CMAKE_REQUIRED_QUIET``
  execute quietly without messages
#]=======================================================================]

include_guard(GLOBAL)

macro(GET_MACRO_VALUE SYMBOL FILE VARIABLE)
  if(CMAKE_C_COMPILER_LOADED)
    __GET_MACRO_VALUE_IMPL("${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeTmp/GetMacroValue.c" "${SYMBOL}" "${FILE}" "${VARIABLE}" )
  elseif(CMAKE_CXX_COMPILER_LOADED)
    __GET_MACRO_VALUE_IMPL("${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeTmp/GetMacroValue.cxx" "${SYMBOL}" "${FILES}" "${VARIABLE}" )
  else()
    message(FATAL_ERROR "GET_MACRO_VALUE needs either C or CXX language enabled")
  endif()
endmacro()

macro(__replace_semicolon dest src)
    string (REPLACE ";" "$<SEMICOLON>" ${dest} "${${src}}")
endmacro()

macro(__GET_MACRO_VALUE_IMPL SOURCEFILE SYMBOL FILE VARIABLE)
    set(TARGET_CACHE_FLAGS -DSYMBOL:STRING=${SYMBOL})
    if (CMAKE_REQUIRED_DEFINITIONS)
        __replace_semicolon(GENERATOR_CMAKE_REQUIRED_DEFINITIONS CMAKE_REQUIRED_DEFINITIONS)
        list(APPEND TARGET_CACHE_FLAGS -DCMAKE_REQUIRED_DEFINITIONS:INTERNAL=${GENERATOR_CMAKE_REQUIRED_DEFINITIONS})
    endif()
    if (CMAKE_REQUIRED_INCLUDES)
        __replace_semicolon(GENERATOR_CMAKE_REQUIRED_INCLUDES CMAKE_REQUIRED_INCLUDES)
        list(APPEND TARGET_CACHE_FLAGS -DCMAKE_REQUIRED_INCLUDES:INTERNAL=${GENERATOR_CMAKE_REQUIRED_INCLUDES})
    endif()

    list(APPEND TARGET_CACHE_FLAGS -DFILE:INTERNAL=${FILE})

    if(NOT CMAKE_REQUIRED_QUIET)
      message(STATUS "Getting value of macro ${SYMBOL}")
    endif()

    #message(STATUS ">${TARGET_CACHE_FLAGS}")

    set(work_dir    ${CMAKE_BINARY_DIR}/GetMacroValue_${SYMBOL})
    set(cmd ${CMAKE_COMMAND}
            --debug-trycompile
            -DCMAKE_CXX_COMPILER=g++
            -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_BINARY_DIR}/${SYMBOL}_VALUE
            ${TARGET_CACHE_FLAGS}
            ${PROJECT_SOURCE_DIR}/cmake/GetMacroValue.subproject)

    file(MAKE_DIRECTORY ${work_dir})

    # Two comands, else cmake trys to run them in parallel
    execute_process(
        COMMAND ${cmd}
        WORKING_DIRECTORY ${work_dir}
    )
    execute_process(
        COMMAND make install
        WORKING_DIRECTORY ${work_dir}
    )

    # test project exports its result, so get it
    find_package(GetMacroValue_${SYMBOL}
        HINTS ${work_dir})

    if(MACRO_VALUE__${SYMBOL})
      if(NOT CMAKE_REQUIRED_QUIET)
        message(STATUS "Looking for macro value ${SYMBOL} - ${MACRO_VALUE__${SYMBOL}}")
      endif()
      set(${VARIABLE} ${MACRO_VALUE__${SYMBOL}} CACHE INTERNAL "Macro value ${SYMBOL}")
    else()
      if(NOT CMAKE_REQUIRED_QUIET)
        message(STATUS "Looking for macro value ${SYMBOL} - not found")
      endif()
      set(${VARIABLE} "" CACHE INTERNAL "Macro value ${SYMBOL}")
    endif()
endmacro()
