function(CREATE_OD_SOURCES OD_EDS OUTPUT_PATH OUTPUT_FILES_VAR)
    if (NOT Mono_EXECUTABLE)
        message(FATAL_ERROR "MONO not found! MONO_EXECUTABLE variable are empty!")
    endif()

    get_filename_component(DIR ${OUTPUT_PATH} NAME)
    set(output_files ${OUTPUT_PATH}/CO_OD.h ${OUTPUT_PATH}/CO_OD.c)
    add_custom_command(OUTPUT ${output_files}
        COMMAND
            mkdir -p ${OUTPUT_PATH}
        COMMAND
            ${Mono_EXECUTABLE} ${CANOPEN_BOOLOADER_OD_src_generator} ${OD_EDS} ${OUTPUT_PATH}
        COMMAND
            sed -i "'s|CO_NO_LSS_SERVER               0|CO_NO_LSS_SERVER               1|'" ${OUTPUT_PATH}/CO_OD.h
        DEPENDS ${Mono_EXECUTABLE} ${OD_src_generator_tgt} ${OD_EDS}
    )
    add_custom_target(generate_OD_${DIR} DEPENDS ${output_files})
    set(${OUTPUT_FILES_VAR} ${output_files} PARENT_SCOPE)
endfunction()
