function(CREATE_HYBRID_BOOTLOADER_AND_APP
        OUTPUT_TARGET BOOTLOADER_TARGET APP_ELF APP_SECTION_NAME APP_ORIGIN_FILE)
    get_filename_component(app_name ${APP_ELF} NAME)
    add_custom_command(OUTPUT ${OUTPUT_TARGET}.elf
        COMMAND ${CMAKE_OBJCOPY}
            -O binary ${APP_ELF} ${app_name}.bin
        COMMAND ${CMAKE_OBJCOPY}
            --add-section .${APP_SECTION_NAME}=${app_name}.bin
            --set-section-flags .${APP_SECTION_NAME}=alloc,load,readonly,code
            --change-section-address .${APP_SECTION_NAME}=`cat ${APP_ORIGIN_FILE}`
            $<TARGET_FILE:${BOOTLOADER_TARGET}> ${OUTPUT_TARGET}
        DEPENDS ${BOOTLOADER_TARGET} ${APP_BIN} ${APP_ORIGIN_FILE}
    )
    add_custom_target(${OUTPUT_TARGET} ALL DEPENDS ${OUTPUT_TARGET}.elf)
endfunction()

function(CREATE_HYBRID_APP_AND_BOOTLOADER
        OUTPUT_TARGET APP_ELF BOOTLOADER_TARGET BOOTLOADER_SECTION_NAME
        BOOTLOADER_ORIGIN)
    set(btlr_bin_file    $<TARGET_FILE:${BOOTLOADER_TARGET}>.bin)
    add_custom_command(OUTPUT ${OUTPUT_TARGET}.elf
        COMMAND ${CMAKE_OBJCOPY}
            -O binary $<TARGET_FILE:${BOOTLOADER_TARGET}> ${btlr_bin_file}
        COMMAND ${CMAKE_OBJCOPY}
            --add-section .${BOOTLOADER_SECTION_NAME}=${btlr_bin_file}
            --set-section-flags .${BOOTLOADER_SECTION_NAME}=alloc,load,readonly,code
            --change-section-address .${BOOTLOADER_SECTION_NAME}=${BOOTLOADER_ORIGIN}
            ${APP_ELF} ${OUTPUT_TARGET}
        DEPENDS ${APP_ELF} ${BOOTLOADER_TARGET}
    )
    add_custom_target(${OUTPUT_TARGET} ALL DEPENDS ${OUTPUT_TARGET}.elf)
endfunction()
