/*!
 *  \file
 *  \brief Bootloader main
 */

#include <cstring>
#include <limits>

#include "CANopen.h"

/*!
 * \ingroup Core
 * @{
 */

#include "BoardInit.h"
#include "CanError.h"
#include "Debug.h"
#include "LSS/LSSClient.h"
#include "LSS/LSSIdentity.h"
#include "LinkInfoStorage.h"
#include "OD_Extantions.h"
#include "app_loader.h"
#include "softreset.h"

#include "DuplicationControl.h"

#ifndef OD_NMTStartup
#error "The macro 'DOD_NMTStartup' mast be defined"
#endif

#ifndef OD_producerHeartbeatTime
#error "The macro 'DOD_producerHeartbeatTime' mast be defined"
#endif

#if APP_AUTOBOOT_DELAY_SEC == 0
#error "APP_AUTOBOOT_DELAY_SEC mast never be 0"
#endif

// нужно заполнить эту структуру, ибо драйвер не делает этого
CanTxMsgTypeDef txMsg{
    .IDE = CAN_ID_STD,  // standart id length 11 bit
    .RTR = CAN_RTR_DATA // all frame is data
};
CanRxMsgTypeDef rxMsg;

CAN_HandleTypeDef can1 {
#if defined(STM32F1) || defined(STM32L4)
  .Instance = CAN1,
#elif defined(STM32F3)
  .Instance = CAN,
#endif
  .pTxMsg = &txMsg, .pRxMsg = &rxMsg
};

// Глобально по тому, что после инициализации он должен работать постоянно
// нет способа его закрыть
LSSClient lSSClient;

/**
 * @brief Disable can module
 */
static void disableCAN() {
  HAL_NVIC_DisableIRQ(CAN1_RX0_INTERRUPTS);
  HAL_NVIC_DisableIRQ(CAN1_TX_INTERRUPTS);
  CO_delete((int32_t)&can1); // shutdown can for bootloader
  HAL_CAN_DeInit(CO->CANmodule[0]->hcan);
  HAL_NVIC_ClearPendingIRQ(CAN1_RX0_INTERRUPTS);
  HAL_NVIC_ClearPendingIRQ(CAN1_TX_INTERRUPTS);
}

static void print_network_config(const LinkInfo &li) {
  DEBUG_PRINTF("linkinfo{\n"
               "\t.node_ID=%u, .bitrate=%u\n"
               "}",
               li.node_ID, li.bitrate);
}

/*!
 * \brief Try start application
 *
 * Verify application integrity and start it, If Application not present or
 * broken, return
 */
static bool tryStartApplication() {
  auto app = App_loader::getApplication();
  if (app->verify()) {
    if (CO->CANmodule) {
      // if module initialised
      disableCAN();
      duplication.destroy();
    }
    DEBUG_MSG("Jump to application...");
    app->start();
    return true;
  }
  return false;
}

/*!
 * @brief Load LSS identity
 *
 * If an application found LSS uses it's information to construct LSS ID, else
 * use default bootloader LSS ID
 */
static bool loadIdentity() {
  bool res;
  auto app = App_loader::getApplication();
  LSSIdentity identity;
  if ((res = app->verify())) {
    identity = LSSIdentity::fromApplication(app);
    SetFlashError(FlashStatus::READY_TO_START);
    DEBUG_MSG("Identity loaded:");
  } else {
    SetFlashError(FlashStatus::INVALID_PROGRAM);
    DEBUG_MSG("Using default identity:");
  }
  identity.Save();
  DEBUG_PRINTF("LSSIdentity{\n"
               "\t.vendorID=0x%lX\n"
               "\t.productCode=0x%lX\n"
               "\t.revisionNumber=0x%lX\n"
               "\t.serialNumber=%lu\n}",
               identity.vendorID, identity.productCode, identity.revisionNumber,
               identity.serialNumber);
  return res;
}

static void CANopenInit(uint8_t nodeID) {
  auto error = CO_CANopenInit(nodeID);
  if (error != CO_ERROR_NO) {
    DEBUG_PRINTF("Error %d during CO_CANopenInit(), rebooting...", error);
    SoftReset();
  }

  if (!registerSDOExtantions(CO->SDO[0])) {
    DEBUG_MSG("Failed to register some OD extantions, rebooting...");
    SoftReset();
  }
}

/*!
 * \brief Reset CAN communication
 *
 * Executes at power on boot and after CANopen NMI RESET_COMMUNICATION command
 * \param linkInfo Communication parameters, if NODE-ID set to
 * CO_LSS_NODE_ID_ASSIGNMENT this function waits new NODE-ID assignments by
 * LSS
 */
static void reset_communication(LinkInfo *linkInfo) {
  DEBUG_MSG("Resetting communication...");

  CO_new(); // создание всех структур
  if (CO_CANinit((int32_t)&can1, linkInfo->bitrate) !=
      CO_ERROR_NO) { // инициализация железа
    if (APP_AUTOBOOT_DELAY_SEC >= 0) {
      DEBUG_MSG("Warn: Failed to init CAN hardware, but autostart defined, "
                "skipping...");
    } else {
      DEBUG_MSG("Error: Failed to init CAN hardware, reset!");
      SoftReset();
    }
  }

  {
    auto app_found = loadIdentity();
    auto app = App_loader::getApplication();
    if (!linkInfo->checkConfigured() && app_found &&
        !app->getLSSFastScanEnabled()) {
      auto stored_config = app->getStoredLinkInfo();
      if (stored_config.checkConfigured()) {
        DEBUG_MSG("Loading network settings from flash...");
        *linkInfo = stored_config;
        LinkInfoStorage.store(stored_config);
        print_network_config(stored_config);
      } else {
        DEBUG_MSG("Stored LSS config invalid... skip");
      }
    }
  }

  // настройка LSS клиента
  lSSClient.configure(CO->CANmodule[0], CO->LSSslave, linkInfo);
  lSSClient.setEnabled(true);

  CO_CANsetNormalMode(CO->CANmodule[0]); // разрешение работы CAN-модуля

  HAL_NVIC_EnableIRQ(CAN1_RX0_INTERRUPTS);
  HAL_NVIC_EnableIRQ(CAN1_TX_INTERRUPTS);

  if (linkInfo->node_ID == CO_LSS_NODE_ID_ASSIGNMENT) {
    // Если приложение валидно, и устройство сброшено, может быть доступен
    // автостарт, иначе ни какого автостарта
    int32_t start_delay =
        App_loader::getApplication()->verify() ? APP_AUTOBOOT_DELAY_SEC : -1;

    // ожидание присвоения адреса по LSS, эта операция обновляет linkInfo, а
    // также сохраняет если пришла соответствующая команда
    DEBUG_MSG("**AWAITING LSS node id assignment**");
    bool assign =
        lSSClient.waitAssignment(CO_LSS_NODE_ID_ASSIGNMENT, start_delay);
    if (!assign) {
      // вышел таймаут, приложение валидно, как раньше проверено, можно
      // запускать
      DEBUG_PRINTF(
          "No LSS communication in %d seconds, autostarting application...",
          APP_AUTOBOOT_DELAY_SEC);
      tryStartApplication();
      DEBUG_MSG("Incorrect application, start aborted!");
      SoftReset();
    } else {
      linkInfo->update_crc();
      duplication.NetworkConfigChanged(*linkInfo);
    }
  } else {
    // сюда мы можем попасть только если приложение осознанно ушло в
    // перезагрузку - ни какого атостарта, инициализируем загрузчик до конца
    auto res = CO_CANmodule_setBitrate(CO->CANmodule[0], linkInfo->bitrate);
    if (res == CO_ERROR_ILLEGAL_ARGUMENT) {
      // failed to set module speed, reset config, reboot
      DEBUG_MSG("Incorrect linkinfo, restore defaults, reboot...");
      linkInfo->resetDefaults();
      duplication.NetworkConfigChanged(*linkInfo);
      LinkInfoStorage.store(*linkInfo);
      SoftReset();
    }
  }

  // запуск всех остальных служб загрузчика
  CANopenInit(linkInfo->node_ID);

  // LSS only enabled if CO_NMT_PRE_OPERATIONAL or CO_NMT_STOPPED
  // else error returns at any LSS requests
  CO_NMT_initCallback(CO->NMT, [](CO_NMT_internalState_t state) {
    duplication.CANStateChanged(state);
    if (state == CO_NMT_STOPPED) {
      // CO_NMT_STOPPED disables SDO server, so, avoid it
      CO->NMT->operatingState = state = CO_NMT_PRE_OPERATIONAL;
    }
    lSSClient.setEnabled(state == CO_NMT_PRE_OPERATIONAL);
  });
}

/*!
 * \brief Get tick timer difference betwen calls
 * \return
 */
static uint32_t getTickDiff(uint32_t &prevTickValue) {
  auto newTick = HAL_GetTick();
  int32_t diff = newTick - prevTickValue;
  prevTickValue = newTick;
  if (diff < 0)
    diff = -diff;

  return static_cast<uint32_t>(diff);
}

#define START_CANOPEN 1

/*!
 * \brief Function process CanOpen stack
 */
static void processCO(LinkInfo &linkInfo, uint32_t &timerValue) {
  auto tickdiff = getTickDiff(timerValue);
  auto cmd = CO_process(CO,
                        tickdiff > std::numeric_limits<uint16_t>::max()
                            ? std::numeric_limits<uint16_t>::max()
                            : static_cast<uint16_t>(tickdiff),
                        nullptr);

  switch (cmd) {
  case CO_RESET_COMM: // Application must provide communication reset.
    __disable_irq();
    DEBUG_MSG("NMT RESET COM ressived");
    reset_communication(&linkInfo);
    __enable_irq();
    break;
  case CO_RESET_APP: // Application must provide complete device reset
  {
    auto clear_link_info = LinkInfo();
    duplication.NetworkConfigChanged(clear_link_info);
    __disable_irq();
    // force LSS obtain address after boot
    LinkInfoStorage.store(clear_link_info);
    DEBUG_MSG("NMT RESET APP ressived, rebooting...");
    SoftReset();
  } break;
  case CO_RESET_NOT: // no action
    break;
  case CO_RESET_QUIT: // Application must quit, no reset of microcontroller
                      // ignore - it's bootloader
    DEBUG_MSG("NMT RESET QUIT ressived, ignored.");
    break;
  }
}

static void onNetworkConfigChanged(void *data, size_t size) {
  if (size != sizeof(LinkInfo)) {
    DEBUG_PRINTF("Reserv: invalid Link info size: %u instead %u", size,
                 sizeof(LinkInfo));
    return;
  }

  auto nli = reinterpret_cast<LinkInfo *>(data);
  if (!nli->check_crc()) {
    DEBUG_MSG("Reserv: Reject update network config: broken data (CRC)");
    return;
  }

  DEBUG_MSG("Reserv: Network settings changed:");
  print_network_config(*nli);
  LinkInfoStorage.store(*nli);
}

static void onShitchWorkingMode() {
  DEBUG_MSG("Reserv: Starting application...");

  if (!tryStartApplication()) { // app starts there, or error
    DEBUG_MSG("Reserv: Incorrect application, start aborted!");
  }
}

static void sendNetworkConfig() {
  auto nli = lSSClient.getAssigedLinkInfo();
  nli.update_crc();
  duplication.NetworkConfigChanged(nli);
}

/*!
 * \brief Bootloader entry point
 * \return Never
 */
int main(void) {
  InitBoard();
  ClearErrorLog();

  duplication.onNetconfig = onNetworkConfigChanged;
  duplication.onShitchWorkingMode = onShitchWorkingMode;

  // if this is reserv hw kit - wait there before boot
  duplication.StartBarier(false);

  if (duplication.IsReservChannel()) {
    DEBUG_MSG("Starting CANopenNode Main bootloader...");
  } else {
    DEBUG_MSG("Starting CANopenNode Reserv bootloader...");
  }

  // Load networc config, reset if invalid
  auto linkInfo = LinkInfoStorage.load();
  if (!linkInfo.checkConfigured()) {
    DEBUG_MSG("No previs configuration found");
    linkInfo.resetDefaults();
    LinkInfoStorage.store(linkInfo);
  } else {
    DEBUG_MSG("Node configured");
    print_network_config(linkInfo);
  }

  __enable_irq();

#if START_CANOPEN
  // Запуск CanOpenNode
  reset_communication(&linkInfo);
#endif

#if START_CANOPEN

  uint32_t timerValue = HAL_GetTick();
  while (1) {
    if (lSSClient.doCheckReconfigured(CO->NMT->nodeId, CANopenInit)) {
      sendNetworkConfig();
    }

    processCO(linkInfo, timerValue);

    if (OD_programControl.programControl1) {
      DEBUG_MSG("**Application start request**");
      if (!tryStartApplication()) { // app starts there, or error
        DEBUG_MSG("Incorrect application, start aborted!");
        OD_programControl.programControl1 = 0;
        SetFlashError(FlashStatus::INVALID_PROGRAM);
      }
    }

    duplication.pool();

    if (OD_reservationControl.startReserv1) {
      sendNetworkConfig();
      disableCAN();
      duplication.destroy();
      while (true) {
        __WFI(); // halt
      }
    }
  }

#endif

  SoftReset();
}

/*! @} */
