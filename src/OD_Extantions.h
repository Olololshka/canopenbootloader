#ifndef OD_EXTATNIONS_H
#define OD_EXTATNIONS_H

#include "CANopen.h"
#include <stdint.h>

/*! \defgroup OD Object dictionary
 * @{
 *
 * \brief Object Dictionary extantions (handlers)
 *
 * Any object dictionar node can have extantion, which can change the behavior
 * when reading or writing. To do this, you mast register the structure
 * **CO_OD_extension_t** that contains the callback function that will be called
 * when the node is accessed.
 * The extension is registeres to the index of the Object Dictionary and must
 * itself manage the subindexes.
 *
 * If a large data array (DOMAIN) is processed, the callback function will be
 * called several times, to store the state between the invocations, you can use
 * the object pointed to by the **object** field of the **CO_OD_extension_t**
 * structure, what will be passed af part of argument **CO_ODF_arg_t**
 *
 * For examle:
 * \code
 * registerSDOExtantion(CO->SDO, my_OD_index,
 *  [](CO_ODF_arg_t *ODF_arg) -> CO_SDO_abortCode_t {
 *      check_subindex(ODF_arg->subIndex); // if subindex defined?
 *      if (ODF_arg->reading) {
 *          // read from OD
 *          ODF_arg->firstSegment; // is first segment
 *          ODF_arg->dataLengthTotal; // data size (can may overrided)
 *          ODF_arg->dataLength; // data in current buffer
 *          ODF_arg->data; // data buffer
 *          // is this call writes all data to
 *          ODF_arg->lastSegment;
 *          return <any of CO_SDO_abortCode_t>
 *      } else {
 *          // write to OD
 *          ...
 *          return <any of CO_SDO_abortCode_t>
 *      }
 * });
 * \endcode
 */

/*!
 * \brief Register new OD extantion by index
 * \param SDO pointer to current SDO struct
 * \param index Index of OD entry
 * \param extantion Const reference to CO_OD_extension_t struct, what contains
 * extantion arguments
 * \return **false**, if OD entry with target index not exists, else **true**
 */
bool registerSDOExtantion(CO_SDO_t *SDO, uint16_t index,
                          const CO_OD_extension_t &extantion);

/*!
 * \brief register my OD extantions
 * \param SDO pointer to current SDO struct
 * \return **true** on success
 */
bool registerSDOExtantions(CO_SDO_t *SDO) __attribute__((weak));

/*! @} */

#endif // OD_EXTATNIONS_H
