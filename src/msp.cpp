/*!
 *  \file MCU-specific callbacks. Its overrides HAL's default weak empty
 * realizations.
 */

#include "hw_includes.h"

/*!
 * \ingroup BootloaderHW
 *  @{
 */

/**
 * @brief CAN bus connect
 *
 * If defined CAN_ENABLE_PIN > 0 use this pin number
 * (+CAN_ENABLE_PORT +CAN_ENABLE_PORT_INVERT) to control can transiver
 * connection to can bus
 *
 * @param enable - connect or disconnect CAN bus
 */
static void HAL_CAN_enable(bool enable) {
#if defined(CAN_ENABLE_PIN) && (CAN_ENABLE_PIN > -1)

#define _gen_port_stringp(l) GPIO##l
#define _gen_port_string(l) _gen_port_stringp(l)

#define _gen_clk_enp(p) __HAL_RCC_GPIO##p##_CLK_ENABLE
#define _gen_clk_en(pv) _gen_clk_enp(pv)

#define _gen_clk_enpin(pin) 1 << pin

  if (enable) {
    _gen_clk_en(CAN_ENABLE_PORT)();
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = _gen_clk_enpin(CAN_ENABLE_PIN),
                                     .Mode = GPIO_MODE_OUTPUT_PP,
                                     .Pull = GPIO_NOPULL,
                                     .Speed = GPIO_SPEED_FREQ_LOW};

    HAL_GPIO_Init(_gen_port_string(CAN_ENABLE_PORT), &GPIO_InitStruct);
    HAL_GPIO_WritePin(_gen_port_string(CAN_ENABLE_PORT),
                      _gen_clk_enpin(CAN_ENABLE_PIN),
                      CAN_ENABLE_PORT_INVERT ? GPIO_PIN_RESET : GPIO_PIN_SET);
  } else {
    HAL_GPIO_WritePin(_gen_port_string(CAN_ENABLE_PORT),
                      _gen_clk_enpin(CAN_ENABLE_PIN),
                      CAN_ENABLE_PORT_INVERT ? GPIO_PIN_SET : GPIO_PIN_RESET);
  }

#undef _gen_port_stringp
#undef _gen_port_string
#undef _gen_clk_enp
#undef _gen_clk_en
#undef _gen_clk_enpin

#endif
}

#if defined(STM32L4)
void HAL_MspInit(void) {
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();
}
#endif

/*!
 *  \brief HAL_CAN_MspInit - This callback function must initialize target CAN
 * module
 *
 * Initialization must contains:
 * - Enable CAN module clocking
 * - Enable clocking GPIO port what's pins will be osed for CAN io
 * - Configure pins mode
 * - Select pin remap, if used
 *
 * This callback used by HAL HAL_CAN_Init() function
 */
extern "C" void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan) {
#if defined(STM32F103xC)
  if (hcan->Instance == CAN1) {
    /*##-1- Enable peripherals and GPIO Clocks
     * #################################*/

    /* Enable GPIO clock ****************************************/
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /* CAN1 Periph clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    /*##-2- Configure peripheral GPIO
     * ##########################################*/

    // TX - output
    GPIO_InitTypeDef GPIO_InitStruct{
        .Pin = GPIO_PIN_9,
        .Mode = GPIO_MODE_AF_PP,
        .Pull = GPIO_NOPULL,
        .Speed = GPIO_SPEED_FREQ_HIGH,
    };
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    // RX - input
    GPIO_InitStruct.Pin = GPIO_PIN_8;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_INPUT;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*##-3- Remap can pins to PB8 PB9
     * ##########################################*/
    /* CAN1 TX GPIO pin configuration */

    __HAL_RCC_AFIO_CLK_ENABLE();
    __HAL_AFIO_REMAP_CAN1_2();

    // loopback mode
    // hcan->Init.Mode = CAN_MODE_NORMAL;
  }
#elif defined(STM32F343xC)
  if (hcan->Instance == CAN) {
    /*##-1- Enable peripherals and GPIO Clocks
     * #################################*/

    /* Enable GPIO clock ****************************************/
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /* CAN1 Periph clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    /*##-2- Configure peripheral GPIO
     * ##########################################*/

    // TX - output
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = GPIO_PIN_9,
                                     .Mode = GPIO_MODE_AF_PP,
                                     .Pull = GPIO_PULLUP,
                                     .Speed = GPIO_SPEED_FREQ_HIGH,
                                     .Alternate = GPIO_AF9_CAN};
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    // RX - input
    GPIO_InitStruct.Pin = GPIO_PIN_8;
    GPIO_InitStruct.Mode =
        GPIO_MODE_AF_PP; // Этоне ошибка, и правда надо этот режим
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  }
#elif defined(STM32L433xC)
  if (hcan->Instance == CAN1) {
    /*##-1- Enable peripherals and GPIO Clocks
     * #################################*/

    /* Enable GPIO clock ****************************************/
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /* CAN1 Periph clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    /*##-2- Configure peripheral GPIO
     * ##########################################*/

    // RX/TX - AF9
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = GPIO_PIN_11 | GPIO_PIN_12,
                                     .Mode = GPIO_MODE_AF_PP,
                                     .Pull = GPIO_PULLUP,
                                     .Speed = GPIO_SPEED_FREQ_HIGH,
                                     .Alternate = GPIO_AF9_CAN1};
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  }
#endif

  HAL_CAN_enable(true);
}

/*!
 *  \brief HAL_CAN_MspDeInit - This callback function must deinit target CAN
 * module
 *
 * Deinitialization must contains:
 * - Disable CAN module clocking
 * - Deinit GPIO bins, used by CAN module
 */
extern "C" void HAL_CAN_MspDeInit(CAN_HandleTypeDef *hcan) {
#if defined(STM32F103xC)
  if (hcan->Instance == CAN1) {
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8 | GPIO_PIN_9);

    __HAL_RCC_CAN1_CLK_DISABLE();
    __HAL_RCC_GPIOB_CLK_DISABLE();
  }
#elif defined(STM32F343xC)
  if (hcan->Instance == CAN) {
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8 | GPIO_PIN_9);

    __HAL_RCC_CAN1_CLK_DISABLE();
    __HAL_RCC_GPIOB_CLK_DISABLE();
  }
#elif defined(STM32L433xC)
  if (hcan->Instance == CAN1) {
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11 | GPIO_PIN_12);

    __HAL_RCC_CAN1_CLK_DISABLE();
    __HAL_RCC_GPIOA_CLK_DISABLE();
  }
#endif

  HAL_CAN_enable(false);
}

static void enable_UART_IRQ(IRQn_Type irqn) {
  static constexpr int UART_IT_PRIO = 6;

  /* Peripheral interrupt init */
  HAL_NVIC_SetPriority(irqn, UART_IT_PRIO, 0);
  HAL_NVIC_EnableIRQ(irqn);
}

static void disable_UART_IRQ(IRQn_Type irqn) { HAL_NVIC_DisableIRQ(irqn); }

void HAL_UART_MspInit(UART_HandleTypeDef *huart) {

#if defined(STM32L433xC)
  if (huart->Instance == USART1) {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART1_CLK_ENABLE();

    // RX/TX - AF7
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = GPIO_PIN_9 | GPIO_PIN_10,
                                     .Mode = GPIO_MODE_AF_PP,
                                     .Pull = GPIO_PULLUP,
                                     .Speed = GPIO_SPEED_FREQ_MEDIUM,
                                     .Alternate = GPIO_AF7_USART1};
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    enable_UART_IRQ(USART1_IRQn);
  } else if (huart->Instance == USART2) {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART2_CLK_ENABLE();

    // RX/TX - AF7
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = GPIO_PIN_2 | GPIO_PIN_3,
                                     .Mode = GPIO_MODE_AF_PP,
                                     .Pull = GPIO_PULLUP,
                                     .Speed = GPIO_SPEED_FREQ_MEDIUM,
                                     .Alternate = GPIO_AF7_USART2};
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    enable_UART_IRQ(USART2_IRQn);
  } else if (huart->Instance == USART3) {
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_USART3_CLK_ENABLE();

    // RX/TX - AF7
    GPIO_InitTypeDef GPIO_InitStruct{.Pin = GPIO_PIN_10 | GPIO_PIN_11,
                                     .Mode = GPIO_MODE_AF_PP,
                                     .Pull = GPIO_PULLUP,
                                     .Speed = GPIO_SPEED_FREQ_MEDIUM,
                                     .Alternate = GPIO_AF7_USART3};
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    enable_UART_IRQ(USART3_IRQn);
  }
#elif RESERV_DETECT_PIN > -1
#error "Add uart config for unknown CPU!"
#endif
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *huart) {
#if defined(STM32L433xC)
  if (huart->Instance == USART1) {
    disable_UART_IRQ(USART1_IRQn);

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9 | GPIO_PIN_10);

    __HAL_RCC_USART1_CLK_DISABLE();
  } else if (huart->Instance == USART2) {
    disable_UART_IRQ(USART2_IRQn);

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2 | GPIO_PIN_3);

    __HAL_RCC_USART2_CLK_DISABLE();
  } else if (huart->Instance == USART3) {
    disable_UART_IRQ(USART3_IRQn);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_10 | GPIO_PIN_11);

    __HAL_RCC_USART3_CLK_DISABLE();
  }
#elif RESERV_DETECT_PIN > -1
#error "Add uart config for unknown CPU!"
#endif
}

/*! @} */
