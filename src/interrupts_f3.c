#include "CANopen.h"

/*! \ingroup Utils Utility and halpers functions
 * @{
 */

/*!
 * \brief CAN module RX Interrupt header
 */
void /* interrupt */ CAN_RX0_IRQHandler(void) {
  CO_CANinterrupt_Rx(CO->CANmodule[0]);
}

/*!
 * \brief CAN module TX Interrupt header
 */
void /* interrupt */ CAN_TX_IRQHandler(void) {
  CO_CANinterrupt_Tx(CO->CANmodule[0]);
}

/*! @} */
