/*!
 * \file
 * \brief Implementation of AES encriptor fasade for raw flash writer
 */

#ifndef AESFLASHWRITER_H
#define AESFLASHWRITER_H

#include "iflashwriter.h"

#include "app_loader.h"

#include "AESStreamDeCrypter.h"

/*! \ingroup Crypto
 * @{
 */

//! \brief Application header size
#define APP_HEADER_SIZE (sizeof(struct App_loader))

/*!
 * \ingroup Crypto
 * \brief Template for AES decriptor fasade for write to flash
 *
 * \param TKeyGen class with static method key(), what can convert application
 * header to AES decription key and has static value key_size, what contains
 * generated key size
 *
 * Example:\code
 *
 * RawFlashWriter fw;
 * AESFlashWriter<SHA2ToAESKeyCalculator<224>> aesFw(fw);
 *
 * aesFw.setBaseAddress(...);
 * aesFw.startTransaction();
 * aesFw.write(...);
 * aesFW.finalise();
 *
 * \endcode
 */
template <typename TKeyGen> class AESFlashWriter : public IFlashWriter {
  static_assert(APP_HEADER_SIZE == 9 * sizeof(uint32_t),
                "HEADER size incorrect");

public:
  /*!
   * \brief Constructor
   * \param parent_writer next stage flash writer of type IFlashWriter
   */
  inline AESFlashWriter(IFlashWriter &parent_writer)
      : decrypter(parent_writer) {}

  /*!
   * \brief Return base address of writing area
   * \return BAse address of write arrea
   */
  inline uint32_t getBaseAddress() const override {
    return decrypter.getWriter().getBaseAddress();
  }

  /*!
   * \brief Configeres parent_writer to write destination
   * \param value Base adddress of destiontion area, user mast ensure what
   * parent_writer can write this area
   * \return error code of parent_writer.setBaseAddress();
   */
  inline FlashStatus setBaseAddress(const uint32_t value) override {
    return decrypter.getWriter().setBaseAddress(value);
  }

  /*!
   * \brief Prepare writer to accept data
   * Actualy call to parent_writer to prepare it
   * \return error code of parent_writer.startTransaction();
   */
  FlashStatus startTransaction() override {
    return decrypter.getWriter().startTransaction();
  }

  /*!
   * \brief Perform write of encrypted data fragment
   *
   * If offset == 0 data chank transfers to TKeyGen to generate decryption key,
   * so id data size less than APP_HEADER_SIZE error will be thrown.
   *
   * After key generation application header will be writen as-as, no decription
   *
   * Oher calls to this method translates data by AES decripter before write to
   * parent_writer
   * \param offset Offset of this data fragment
   * \param data Pointer to encrypted data buffer
   * \param size Size of encrypted data fragment
   * \return
   *    FlashStatus::DECRYPTION_INSUFFICIENT_DATA - if decription key generation
   * failed or code of paretn_writer.write()
   */
  FlashStatus write(uint32_t offset, const uint8_t *data,
                    size_t size) override {
    if (offset == 0) {
      if (size < APP_HEADER_SIZE) {
        return FlashStatus::DECRYPTION_INSUFFICIENT_DATA;
      }
      AesInit(data);

      // write header itself
      auto err = decrypter.getWriter().write(0, data, APP_HEADER_SIZE);
      if (err != FlashStatus::OK)
        return err;

      // recursive call, to decrypt and write rest of data
      return write(APP_HEADER_SIZE, &data[APP_HEADER_SIZE],
                   size - APP_HEADER_SIZE);
    } else {
      return decrypter.write(offset, data, size);
    }
  }

  /*!
   * \brief Finalise write transaction
   *
   * Encrypt rest of data and write result
   * \return error code of parent_writer.finalise()
   */
  FlashStatus finalise() override {
    auto res = decrypter.flush();
    if (res != FlashStatus::OK)
      return res;
    return decrypter.getWriter().finalise();
  }

  /*!
   * \brief Abort write operation, safly close parent_writer by call abort()
   */
  void abort() override { decrypter.getWriter().abort(); }

private:
  AESStreamDeCrypter<TKeyGen::key_size> decrypter;

  void AesInit(const uint8_t data[APP_HEADER_SIZE]) {
    uint8_t key[TKeyGen::key_size];
    uint8_t const *iv;

    TKeyGen::key(data, APP_HEADER_SIZE, key);
    iv = &data[sizeof(uint32_t)];

    decrypter.reset(key, iv);
  }
};

/*!
 * @}
 */

#endif // AESFLASHWRITER_H
