#ifndef _APP_LOADER_H_
#define _APP_LOADER_H_

#include <stdint.h>
#include <stdlib.h>

#include <LinkInfoStorage.h>

class RawFlashWriter;
class IFlashWriter;

/*!
 * \defgroup Application Application interface
 */

/*!
 * \ingroup Application
 * \brief The App_loader struct
 *
 * Provides ability to check if application present and it's integrity,
 * application storage and some about information
 *
 * Example:\code
 *
 * auto app = App_loader::getApplication();
 * if (app->verify()) {
 *  auto vid = app->get_canopen_vendor_id();
 *  auto pid = ...
 *
 *  app->start(); // will not return from there
 * }
 *
 * \endcode
 */

struct App_loader {
  /*!
   * \brief Factory method to calculate position of application
   * in cpu flash
   *
   * `Application_position = STM32_FLASH_BASE + ALIGN(SIZEOF(BOOLOADER),
   * FLASH_PAGE_SIZE)`
   * \return Pointer to actual position of application as
   * Application struct
   */
  static const App_loader *getApplication();

  /*! Disable default constructor */
  App_loader() = delete;

  /*! Disable copy */
  App_loader(App_loader &) = delete;

  /*!
   * \brief Verify application integrity.
   *
   * Analyze application header to check if application can be started
   * Checklist:
   * - Is application magic number valid?
   * - Is application image start and end aligned by 4 bytes
   * - Is application image end address > start
   * - Is application interrupt vectors table address between image start and
   * end
   * - Is application stack pointer initial value into RAM dress range
   * - Is application entry point (reset vector) into application image address
   * range
   * - Is application image checksum equals defined in header
   *
   * \return True, if application valid. False any of previews check are failed
   */
  bool verify() const; // check application header before start

  /*!
   * \brief Start the application
   *
   * No-return method, what reset stack pointer to application-defined value and
   * jumps to application flash start address
   *
   * \warning Do not start unchecked applications, use verify() to check
   * integrity first!
   */
  void start() const __attribute__((noreturn));

  /*!
   * \brief Calculate and return actual application crc32
   * \return Actual application CRC32, or 0 if application not found
   */
  uint32_t crc() const;

  /*!
   * \brief Get CANopen vendor ID from application header
   *
   * This value mast be world-unique
   * \return CANopen vendor ID
   */
  uint32_t get_canopen_vendor_id() const { return app_canopen_vendor_id; }

  /*!
   * \brief Get CANopen product ID from application header
   *
   * This value vendor specific, but != 0
   * \return CANopen product ID
   */
  uint32_t get_canopen_product_id() const { return app_canopen_product_id; }

  /*!
   * \brief Get Version of application firmware from application header
   *
   * Any value provided by vendor
   * \return Application version
   */
  uint32_t get_version() const { return app_version; }

  /*!
   * \brief Get serial number of device from application settings storage
   *
   * Application provides pointer to it's settings storage.
   * The first field of it must be uint32_t serial_number.
   * This method reads this value. Must be > 0
   * \return serial number of device
   */
  uint32_t get_serial() const;

  /*!
   * \brief Get size of application image in bytes
   * \return size of application
   */
  uint32_t size() const;

  /*!
   * \brief Configure FlashWriter object, setup base flash page to satrt write
   * \param writer FlashWriter instance
   */
  void configureWriter(IFlashWriter &writer) const;

  /*!
   * \brief Chack, if controller have anoth memory to put application image
   *
   * `Avalable_application_size = STM32_FLASH_SIZE - SIZEOF(bootloader) -
   * FLASH_PAGE_SIZE`
   * \param size requested application image size
   * \return **true**, if avalable flash memory > requestd image size
   */
  bool isMemoryAvalable(size_t size) const;

  /*!
   * \brief get pointer to flash, where application settings stored
   * \return pointer to flash area
   */
  void *getSettingsPtr() const;

  /*!
   * \brief gread and return application settings size
   * \return actual size of application settings
   */
  uint32_t getAppSettingsSize() const;

  /*!
   * \brief get status of LSS settings store enabled
   * \return True - enabled
   */
  bool getLSSFastScanEnabled() const;

  /*!
   * \brief get stored link info from app settings
   * \return Stored Link info
   */
  LinkInfo getStoredLinkInfo() const;

private:
  bool verifyHeaderFields() const;

  /// fields order corresponds to application linker script section .header
  volatile uint32_t app_magick; ///< Application magick number
  volatile uint32_t
      app_image_start;             ///< Application image start address in FLASH
  volatile uint32_t app_image_end; ///< Application image end adress in FLASH
  volatile uint32_t app_canopen_vendor_id;  ///< World-unique CANopen Vendor ID
  volatile uint32_t app_canopen_product_id; ///< Vendor-specific product ID
  volatile uint32_t app_version;            ///< Application version
  volatile uint32_t app_crc32;              ///< Application image crc32
  volatile void
      *app_settings; ///< Pointer to application settings stored in flash
  volatile uint32_t app_vectors_pos; ///< Address of application ISR vectors
                                     ///< table copy in FLASH
};

#endif /* _APP_LOADER_H_ */
