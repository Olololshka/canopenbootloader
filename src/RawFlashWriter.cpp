#include <cstring>
#include <limits>

#include "softreset.h"

#include "RawFlashWriter.h"

uint32_t RawFlashWriter::getBaseAddress() const { return baseAddress; }

void RawFlashWriter::unlock() { m_lock = false; }

FlashStatus RawFlashWriter::lock() {
  if (m_lock) {
    return FlashStatus::LOCKED;
  } else {
    m_lock = true;
    return FlashStatus::OK;
  }
}

FlashStatus RawFlashWriter::setBaseAddress(const uint32_t value) {
  if (lock() != FlashStatus::OK) {
    return FlashStatus::LOCKED;
  }
  baseAddress = value;
  unlock();
  return FlashStatus::OK;
}

FlashStatus RawFlashWriter::startTransaction() {
  if (lock() != FlashStatus::OK) {
    abort();
    return startTransaction();
  }
  if (HAL_FLASH_Unlock() == HAL_OK)
    return FlashStatus::OK;
  else {
    abort();
    return FlashStatus::LOCKED;
  }
}

FlashStatus RawFlashWriter::write(uint32_t offset, const uint8_t *data,
                                  size_t size) {
  if (!m_lock) {
    return FlashStatus::SEQUENCE_CORRUPT;
  }
  return cache.write(baseAddress + offset, data, size);
}

FlashStatus RawFlashWriter::finalise() {
  if (!m_lock) {
    return FlashStatus::SEQUENCE_CORRUPT;
  }

  auto res = cache.sync();
  HAL_FLASH_Lock();
  unlock();
  return res;
}

void RawFlashWriter::abort() {
  HAL_FLASH_Lock();
  unlock();
}

FlashStatus RawFlashWriter::Page_cahce::write(uint32_t absolute_address,
                                              const uint8_t *data,
                                              size_t size) {
  auto target_page_base = address2page(absolute_address);
  if (target_page_base == std::numeric_limits<uint32_t>::max())
    return FlashStatus::APPLICATION_TOO_LARGE;

  switch (cache_state) {
  case State::Idle:
    cache_page(target_page_base);
    break;
  case State::Clean:
    if (target_page_base != cached_page_base)
      cache_page(target_page_base);
    break;
  case State::Duty:
    if (target_page_base != cached_page_base) {
      auto res = sync();
      if (res != FlashStatus::OK)
        return res;
      cache_page(target_page_base);
    }
    break;
  }

  auto writen = page_write(absolute_address - cached_page_base, data, size);
  if (writen < size) {
    auto res = sync();
    if (res != FlashStatus::OK)
      return res;
    return write(absolute_address + writen, data + writen, size - writen);
  }
  return FlashStatus::OK;
}

uint32_t RawFlashWriter::Page_cahce::address2page(uint32_t absolute_address) {
  return (!IS_FLASH_PROGRAM_ADDRESS(absolute_address))
             ? std::numeric_limits<uint32_t>::max()
             : absolute_address - absolute_address % FLASH_PAGE_SIZE;
}

void RawFlashWriter::Page_cahce::cache_page(uint32_t absolute_address) {
  std::memcpy(cached_page_data,
              reinterpret_cast<const void *>(absolute_address),
              FLASH_PAGE_SIZE);
  cached_page_base = absolute_address;
  cache_state = State::Clean;
}

FlashStatus RawFlashWriter::Page_cahce::sync() {
  if (cache_state != State::Duty) {
    return FlashStatus::OK;
  }

  FlashStatus status = FlashStatus::OK;
  status = erease_page(cached_page_base);
  if (status != FlashStatus::OK) {
    return status;
  }

  uint32_t destination = cached_page_base;
  uint32_t src_offset = 0;
  size_t to_write = FLASH_PAGE_SIZE;

  while (to_write) {
#if defined(STM32F1) || defined(STM32F3)
    if (to_write > sizeof(uint64_t)) {
      status = flash_write_type<FLASH_TYPEPROGRAM_DOUBLEWORD>(
          src_offset, destination, to_write);
    } else {
      if (to_write < sizeof(uint16_t))
        return FlashStatus::ALIGNMENT_ERROR;

      status = flash_write_type<FLASH_TYPEPROGRAM_HALFWORD>(
          src_offset, destination, to_write);
    }
#elif defined(STM32L4)
    static_assert(FLASH_PAGE_SIZE % FLASH_ROW_SIZE == 0,
                  "Incorrect flash paging!");

    // enother write flash mechanism
    if (to_write >= sizeof(uint64_t)) {
      status = flash_write_type<FLASH_TYPEPROGRAM_DOUBLEWORD>(
          src_offset, destination, to_write);
    } else {
      // imposibly to get there
      SoftReset();
    }

#endif
    if (status != FlashStatus::OK) {
      return status;
    }
  }

  cache_state = State::Clean;
  return FlashStatus::OK;
}

size_t RawFlashWriter::Page_cahce::page_write(uint32_t offset,
                                              const uint8_t *data,
                                              size_t size) {
  if (size > FLASH_PAGE_SIZE - offset) {
    size = FLASH_PAGE_SIZE - offset;
  }

  std::memcpy(&cached_page_data[offset], data, size);
  cache_state = State::Duty;

  return size;
}

FlashStatus RawFlashWriter::Page_cahce::erease_page(uint32_t page_base) {
#if defined(STM32F373xC)
  FLASH_EraseInitTypeDef erease_info{FLASH_TYPEERASE_PAGES, page_base, 1};
#elif defined(STM32L433xC)
  FLASH_EraseInitTypeDef erease_info{FLASH_TYPEERASE_PAGES, FLASH_BANK_1,
                                     (page_base - FLASH_BASE) / FLASH_PAGE_SIZE,
                                     1};
#else
#error "Fill erease_info for target CPU"
#endif
  uint32_t err;

  return (HAL_FLASHEx_Erase(&erease_info, &err) != HAL_OK)
             ? FlashStatus::ERASE_FAIL
             : FlashStatus::OK;
}
