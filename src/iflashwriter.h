#ifndef IFLASHWRITER_H
#define IFLASHWRITER_H

#include <stdint.h>
#include <stdlib.h>

/*!
 * \ingroup Application Flash writer
 * @{
 */

/*!
 * \brief Flash write status codes
 *
 * This codes for ObjectDictionary **[1F57sub1]**
 */
enum class FlashStatus {
  NO_OPERATION = 0, ///< No flash operations was performed
  OK = 1,           ///< Last flash operation was successed
  LOCKED =
      2, ///< Attempt to make a new record before the previous one is completed
  APPLICATION_TOO_LARGE = 3, ///< Flash address out of avalable flash region
  ERASE_FAIL = 4,            ///< Flash erase operation was failed
  WRITE_FAIL = 5,            ///< Flash write operation was failed
  ALIGNMENT_ERROR = 6,       ///< Fize of image not multiple of 2 bytes
  SEQUENCE_CORRUPT = 7,      ///< Flash sequence was broken
  INVALID_PROGRAM = 8, ///< Flash memory contains invalid programm, or empty
  READY_TO_START =
      9, ///< Flash memory contains valid programm, it may be started
  DECRYPTION_INSUFFICIENT_DATA =
      10, ///< Error while decrypting application, header data to small
};

/*!
 * \brief Interface of flash writers
 */
struct IFlashWriter {
  /*!
   * \brief Get writer base address
   *
   * Configuration value
   * \return Address of flash in MCU, or 0, if unconfigured
   */
  virtual uint32_t getBaseAddress() const = 0;

  /*!
   * \brief Set writer base address
   *
   * Set value, what will be use as base flash address
   * \param value Address in MCU flash.
   * \return FlashStatus::OK on success, FlashStatus::LOCKED if another flasing
   * operation not finished
   * \warning Client code mast set flash base address before any write
   * operation. Base address can't be changed while current write operation nit
   * finished of aborted
   */
  virtual FlashStatus setBaseAddress(const uint32_t value) = 0;

  /*!
   * \brief Start new flash write transaction
   *
   * Attempt to start a new flash writing sequence
   * \return FlashStatus::OK, if flash ready to write, else FlashStatus::LOCKED
   */
  virtual FlashStatus startTransaction() = 0;

  /*!
   * \brief Write new data block to flash
   *
   * Use caching of page to reduce read-modfy-write operation
   * Write data performs only, if offset of new data block not in current cached
   * flash page, or if pointer overlaps flash page borger
   * \param offset Start offset to write relative to writer base address
   * \param data pointer to data block
   * \param size data block length
   * \return FlashStatus Error code
   * CODE | Reason
   * ---- | ------
   * FlashStatus::OK | Operation successful
   * FlashStatus::SEQUENCE_CORRUPT | Flash not ready to write (is
   * startTransaction() called?) FlashStatus::ERASE_FAIL | Cant'n erase page
   * before write FlashStatus::ALIGNMENT_ERROR | Something strange, Flash page
   * size not multiple of 2 bytes FlashStatus::WRITE_FAIL | Write failed
   */
  virtual FlashStatus write(uint32_t offset, const uint8_t *data,
                            size_t size) = 0;

  /*!
   * \brief Finalise write operation
   *
   * Performs sync cache, if used.
   * Close MCU flash for write
   * \return Similarly as write()
   */
  virtual FlashStatus finalise() = 0;

  /*!
   * \brief Break current flash transaction
   *
   * Drop cache and close MCU flash for write
   */
  virtual void abort() = 0;

protected:
  IFlashWriter() {}
};

/*! @} */

#endif // IFLASHWRITER_H
