#ifndef IKEYCALCULATOR_H
#define IKEYCALCULATOR_H

#include <stdint.h>
#include <stdlib.h>

enum class KeyGeneratonError {
  OK = 0,                ///< Key generated succesfuly
  INSUFFICIENT_DATA = 1, ///< Input data to short
};

template <size_t KeySize> struct IKeyCalculator {
  virtual KeyGeneratonError getKey(const uint8_t *data, const size_t dsize,
                                   uint8_t okey[KeySize]) = 0;
};

#endif // IKEYCALCULATOR_H
