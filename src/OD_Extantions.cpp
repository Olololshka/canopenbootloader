#include <cstring>
#include <numeric>
#include <utility>

#include "AESFlashWriter.h"
#include "CanError.h"
#include "Debug.h"
#include "RawFlashWriter.h"
#include "SHA2ToKey.h"
#include "app_loader.h"

#include "DuplicationControl.h"

#include "OD_Extantions.h"

#define assert_subindex(subindex)                                              \
  do {                                                                         \
    if (ODF_arg->subIndex != subindex)                                         \
      return CO_SDO_AB_SUB_UNKNOWN;                                            \
  } while (0)

// declare raw flash writer
static RawFlashWriter rawflashWriter;

// declare AES encripted flas writer with SHA2ToAESKeyCalculator<224> key
// generator fasade for raw flash writer
static AESFlashWriter<SHA2ToAESKeyCalculator<224>> AESWriter(rawflashWriter);

// place to hold settings backup during application update
static uint8_t settings_backup[FLASH_PAGE_SIZE];
static void *prev_settings_pos;

static IFlashWriter &selectFlashWriter() {
  switch (
      CO_OD_RAM.applicationImageEncriptionMethod.application1EncriptionMethod) {
  case 1:
    return AESWriter;
  default:
    return rawflashWriter;
  }
}

static void backup_settings(void *actual_settings) {
  prev_settings_pos = actual_settings;
  std::memcpy(settings_backup, actual_settings, sizeof(settings_backup));
}

static FlashStatus restore_settings(void *new_settings_pos) {
  if (new_settings_pos != prev_settings_pos) {

    auto &flashWriter = rawflashWriter;

    FlashStatus flash_status;

    flashWriter.setBaseAddress(reinterpret_cast<uint32_t>(new_settings_pos));

    flash_status = flashWriter.startTransaction();
    if (flash_status != FlashStatus::OK) {
      DEBUG_PRINTF("settings: Error %u occured during flash initialisation",
                   flash_status);
      flashWriter.abort();
      return flash_status;
    }

    flash_status =
        flashWriter.write(0, settings_backup, sizeof(settings_backup));
    if (flash_status != FlashStatus::OK) {
      DEBUG_PRINTF("settings: Error %u occured during flash write",
                   flash_status);
      flashWriter.abort();
      return flash_status;
    }

    flash_status = flashWriter.finalise();
    if (flash_status != FlashStatus::OK) {
      DEBUG_PRINTF("settings: Error %u occured during flash finalisation",
                   flash_status);
      return flash_status;
    }
  }
  return FlashStatus::OK;
}

static CO_SDO_abortCode_t simple_read_OD(CO_ODF_arg_t *ODF_arg) {
  std::memcpy(ODF_arg->data, ODF_arg->ODdataStorage, ODF_arg->dataLength);
  return CO_SDO_AB_NONE;
}

static CO_SDO_abortCode_t
patch_if_duplication_kit_active(CO_ODF_arg_t *ODF_arg) {
  static const char duplication_kit_marker[] = " (Reserv.)";

  if (duplication.IsReservChannel()) {
    std::memcpy(&ODF_arg->data[ODF_arg->dataLength], duplication_kit_marker,
                sizeof(duplication_kit_marker));
    ODF_arg->dataLength += sizeof(duplication_kit_marker);
    ODF_arg->dataLengthTotal += sizeof(duplication_kit_marker);
  }
  return CO_SDO_AB_NONE;
}

template <uint16_t subindex>
static CO_SDO_abortCode_t processFirmwareWrite(CO_ODF_arg_t *ODF_arg) {
  if (ODF_arg->subIndex == 0) {
    return simple_read_OD(ODF_arg);
  }

  assert_subindex(subindex);

  auto app = App_loader::getApplication();
  if (ODF_arg->reading) {
    return CO_SDO_AB_WRITEONLY;
  } else {
    auto &flashWriter = selectFlashWriter();

    FlashStatus flash_status;
    if (ODF_arg->firstSegment) {
      if (!app->isMemoryAvalable(ODF_arg->dataLengthTotal)) {
        DEBUG_PRINTF("No space avalable for application size %u bytes",
                     ODF_arg->dataLengthTotal);
        SetFlashError(FlashStatus::APPLICATION_TOO_LARGE);
        return CO_SDO_AB_OUT_OF_MEM;
      }

      app->configureWriter(flashWriter);

      backup_settings(app->getSettingsPtr());

      flash_status = flashWriter.startTransaction();
      if (flash_status != FlashStatus::OK) {
        DEBUG_PRINTF("Error %u occured during flash initialisation",
                     flash_status);
        flashWriter.abort();
        SetFlashError(flash_status);
        return CO_SDO_AB_DATA_LOC_CTRL;
      }
    }

    flash_status =
        flashWriter.write(ODF_arg->offset, ODF_arg->data, ODF_arg->dataLength);
    if (flash_status != FlashStatus::OK) {
      DEBUG_PRINTF("Error %u occured during flash write", flash_status);
      flashWriter.abort();
      SetFlashError(flash_status);
      return CO_SDO_AB_DATA_LOC_CTRL;
    }

    if (ODF_arg->lastSegment) {
      flash_status = flashWriter.finalise();
      if (flash_status != FlashStatus::OK) {
        DEBUG_PRINTF("Error %u occured during flash finalisation",
                     flash_status);
        SetFlashError(flash_status);
        return CO_SDO_AB_DATA_LOC_CTRL;
      }

      DEBUG_MSG("Flashing complead");
      if (app->verify()) {
        auto flash_status = restore_settings(app->getSettingsPtr());
        if (flash_status == FlashStatus::OK) {
          DEBUG_MSG("Application ready to start!");
          SetFlashError(FlashStatus::READY_TO_START);
        } else {
          DEBUG_MSG("Application updated, but settings corrupted!");
          SetFlashError(flash_status);
        }
      } else {
        DEBUG_MSG("Application format incorrect, blocked");
        SetFlashError(FlashStatus::INVALID_PROGRAM);
      }
    }

    return CO_SDO_AB_NONE;
  }
}

template <uint16_t subindex>
static CO_SDO_abortCode_t processAppEncryptionMethod(CO_ODF_arg_t *ODF_arg) {
  if (ODF_arg->subIndex == 0) {
    return simple_read_OD(ODF_arg);
  }

  assert_subindex(subindex);

  if (ODF_arg->reading) {
    std::memcpy(ODF_arg->data, ODF_arg->ODdataStorage, ODF_arg->dataLength);
  } else {

    if (ODF_arg->dataLength != sizeof(uint32_t)) {
      return CO_SDO_AB_TYPE_MISMATCH;
    }
    uint32_t *v = reinterpret_cast<uint32_t *>(ODF_arg->data);
    if (*v > 1) { // check 0 - no  encryption or 1 - AESWriter
      return CO_SDO_AB_VALUE_HIGH;
    }

    CO_OD_RAM.applicationImageEncriptionMethod.application1EncriptionMethod =
        *v;
  }
  return CO_SDO_AB_NONE;
}

template <uint16_t subindex>
static CO_SDO_abortCode_t processFirmwareCRC32(CO_ODF_arg_t *ODF_arg) {
  if (ODF_arg->subIndex == 0) {
    return simple_read_OD(ODF_arg);
  }

  assert_subindex(subindex);

  if (ODF_arg->reading) {
    // mast return actual crc32 of application or 0 if not present
    auto app_crc = App_loader::getApplication()->crc();

    if (ODF_arg->dataLength != sizeof(app_crc))
      return CO_SDO_AB_DATA_OD;

    std::memcpy(ODF_arg->data, &app_crc, sizeof(app_crc));

    return CO_SDO_AB_NONE;
  } else {
    return CO_SDO_AB_READONLY;
  }
}

template <uint16_t subindex>
static CO_SDO_abortCode_t processApplicationSettings(CO_ODF_arg_t *ODF_arg) {
  if (ODF_arg->subIndex == 0) {
    return simple_read_OD(ODF_arg);
  }

  assert_subindex(subindex);

  auto app = App_loader::getApplication();

  if (!app->verify()) {
    // application invalid, can't work with settings
    return CO_SDO_AB_DATA_OD;
  }

  const auto pSettings = app->getSettingsPtr();
  const auto settingsSize = app->getAppSettingsSize();

  if (ODF_arg->reading) {
    auto src = static_cast<uint8_t *>(pSettings) + ODF_arg->offset;
    ODF_arg->dataLength = std::min<size_t>(
        settingsSize - ODF_arg->dataLengthTotal, ODF_arg->dataLength);

    std::memcpy(ODF_arg->data, src, ODF_arg->dataLength);

    ODF_arg->dataLengthTotal += ODF_arg->dataLength;
    ODF_arg->lastSegment =
        (ODF_arg->offset + ODF_arg->dataLength) == settingsSize;
  } else {
    auto &flashWriter = rawflashWriter;

    FlashStatus flash_status;
    if (ODF_arg->firstSegment) {
      flashWriter.setBaseAddress(reinterpret_cast<uint32_t>(pSettings));

      flash_status = flashWriter.startTransaction();
      if (flash_status != FlashStatus::OK) {
        DEBUG_PRINTF("settings: Error %u occured during flash initialisation",
                     flash_status);
        flashWriter.abort();
        SetFlashError(flash_status);
        return CO_SDO_AB_DATA_LOC_CTRL;
      }
    }

    flash_status =
        flashWriter.write(ODF_arg->offset, ODF_arg->data, ODF_arg->dataLength);
    if (flash_status != FlashStatus::OK) {
      DEBUG_PRINTF("settings: Error %u occured during flash write",
                   flash_status);
      flashWriter.abort();
      SetFlashError(flash_status);
      return CO_SDO_AB_DATA_LOC_CTRL;
    }

    if (ODF_arg->lastSegment) {
      flash_status = flashWriter.finalise();
      if (flash_status != FlashStatus::OK) {
        DEBUG_PRINTF("settings: Error %u occured during flash finalisation",
                     flash_status);
        SetFlashError(flash_status);
        return CO_SDO_AB_DATA_LOC_CTRL;
      }
    }
  }

  return CO_SDO_AB_NONE;
}

//! List of predefined OD extantions
static constexpr std::pair<uint16_t, CO_OD_extension_t> myExtantions[] = {
    //! Pre-defined error field
    {0x1003, CO_OD_extension_t{processErrorLog}},
    //! Manufacturer device name - patch if duplication kit active
    {0x1008, CO_OD_extension_t{patch_if_duplication_kit_active}},
    //! Application write OD entry
    {0x1f50, CO_OD_extension_t{processFirmwareWrite<1>}},
    //! Application image encription method
    {0x1f55, CO_OD_extension_t{processAppEncryptionMethod<1>}},
    //! Application crc32 OD entry
    {0x1f56, CO_OD_extension_t{processFirmwareCRC32<1>}},
    //! Application settings OD entry
    {0x1f5A, CO_OD_extension_t{processApplicationSettings<1>}}};

bool registerSDOExtantion(CO_SDO_t *SDO, const uint16_t index,
                          const CO_OD_extension_t &extantion) {
  auto programm_od_entry = CO_OD_find(SDO, index);
  if (programm_od_entry == 0xFFFFU) {
    return false; /* object does not exist in OD */
  }
  SDO->ODExtensions[programm_od_entry] = extantion;
  return true;
}

bool registerSDOExtantions(CO_SDO_t *SDO) {
  return std::accumulate(
      myExtantions,
      myExtantions + sizeof(myExtantions) / sizeof(myExtantions[0]), true,
      [SDO](bool acumulator, auto pair) {
        return (!acumulator)
                   ? acumulator
                   : registerSDOExtantion(SDO, pair.first, pair.second);
      });
}

/*! @} */
