#ifndef _SHA2_TO_KEY_H_
#define _SHA2_TO_KEY_H_

#include <type_traits>

#include "IKeyCalculator.h"

#include "mbedtls/sha256.h"

/*! \ingroup Crypto
 * @{
 */

/*!
 * \brief Base template for SHA2 hash calculator base
 */
template <int HashType> struct SHA2ToAESKeyCalculatorBase {
  //! \brief size of hash in bytes
  static constexpr size_t hash_size = HashType / 8;

  //! \brief size of generated key
  static constexpr size_t key_size = 128 / 8;
};

template <int HashType> struct SHA2ToAESKeyCalculator {};

/*!
 * \brief Specialisation for SHA224 to key
 */
template <>
struct SHA2ToAESKeyCalculator<224> : public SHA2ToAESKeyCalculatorBase<224> {
  /*!
   * \brief Convert any size of input data to 128 bit key
   * \param data pointer to input data
   * \param dsize size of input data
   * \param okey output 128 bit key
   */
  static void key(const uint8_t *data, const size_t dsize,
                  uint8_t okey[key_size]) {
    uint8_t _digest[32];
    mbedtls_sha256_ret(data, dsize, _digest, true);

    for (uint32_t i = 0; i < key_size - 2; ++i) {
      okey[i] = _digest[i * 2];
    }

    okey[14] = _digest[hash_size - 3];
    okey[15] = _digest[hash_size - 1];
  }
};

/*!
 * \brief Specialisation for SHA256 to key
 */
template <>
struct SHA2ToAESKeyCalculator<256> : public SHA2ToAESKeyCalculatorBase<256> {
  /*!
   * \brief Convert anu size of input data to 128 bit key
   * \param data pointer to input data
   * \param dsize size of input data
   * \param okey output 128 bit key
   */
  static void key(const uint8_t *data, const size_t dsize,
                  uint8_t okey[key_size]) {
    uint8_t _digest[32];
    mbedtls_sha256_ret(data, dsize, _digest, false);

    for (uint32_t i = 0; i < key_size; ++i) {
      okey[i] = _digest[i * 2];
    }
  }
};

/*!
 * @}
 */

#endif // _SHA2_TO_KEY_H_
