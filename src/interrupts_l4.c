#include "CANopen.h"

/*! \ingroup Utils Utility and halpers functions
 * @{
 */

/*!
 * \brief CAN module RX Interrupt header
 */
void /* interrupt */ CAN1_RX0_IRQHandler(void) {
  CO_CANinterrupt_Rx(CO->CANmodule[0]);
}

/*!
 * \brief CAN module TX Interrupt header
 */
void /* interrupt */ CAN1_TX_IRQHandler(void) {
  CAN_HandleTypeDef *hcan = CO->CANmodule[0]->hcan;

  /* Disable Transmit mailbox empty Interrupt */
  __HAL_CAN_DISABLE_IT(hcan, CAN_IT_TME);

  if (hcan->State == HAL_CAN_STATE_BUSY_TX) {
    /* Disable interrupts: */
    /*  - Disable Error warning Interrupt */
    /*  - Disable Error passive Interrupt */
    /*  - Disable Bus-off Interrupt */
    /*  - Disable Last error code Interrupt */
    /*  - Disable Error Interrupt */
    __HAL_CAN_DISABLE_IT(hcan, CAN_IT_EWG | CAN_IT_EPV | CAN_IT_BOF |
                                   CAN_IT_LEC | CAN_IT_ERR);
  }

  if (hcan->State == HAL_CAN_STATE_BUSY_TX_RX) {
    /* Change CAN state */
    hcan->State = HAL_CAN_STATE_BUSY_RX;
  } else {
    /* Change CAN state */
    hcan->State = HAL_CAN_STATE_READY;
  }

  CO_CANinterrupt_Tx(CO->CANmodule[0]);
}

/*! @} */
