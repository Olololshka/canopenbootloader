#ifndef AESFLASHWRITER_H
#define AESFLASHWRITER_H

#include <limits>
#include <stdint.h>

#include "hw_includes.h"

/*! \ingroup Utils
 * @{
 */

/*!
 * \brief Simple pooling timer, provides time counting use \a HAL_GetTick()
 *
 * Requires repeat call PoolingTimer::check() to detect timeout
 *
 * \param T type of counter value - any integer type
 * \param devider Devider of value, provided by HAL_GetTick()
 */
template <typename T, uint32_t devider> struct PoolingTimer {
  /*!
   * \brief Constructor
   * \param timeout initial Value of timeout downcounter
   */
  constexpr PoolingTimer(const T timeout = 0)
      : m_timeout(timeout), enabled(false) {}

  /*!
   * \brief setTimeout Set new value of timeout downcounter
   * \param timeout new value
   */
  void setTimeout(T timeout) { m_timeout = timeout; }

  /*!
   * \brief getTimeout Get current downcounter value
   * \return
   */
  T getTimeout() const { return m_timeout; }

  /*!
   * \brief Check if timeout passed
   *
   * Each call of this method checks if timeout passed by read HAL_GetTick() and
   * compare to preveus value, difference divides by devider
   * \return True - timer finished, false - timer in progress
   */
  bool check() {
    if (!enabled) {
      updateTicks();
      return false;
    }

    uint32_t hal_ticks = HAL_GetTick();
    uint32_t diff =
        (hal_ticks > ticks)
            ? (hal_ticks - ticks)
            : (std::numeric_limits<uint32_t>::max() - ticks + hal_ticks);
    uint32_t dev_diff = diff / devider;

    if (dev_diff > 0) {
      ticks += dev_diff * devider;
      if (m_timeout > 0) {
        while (dev_diff--) {
          --m_timeout;
          if (!m_timeout) {
            return true;
          }
        }
        return false;
      } else if (m_timeout == 0) {
        return true;
      }
    }
    return false;
  }

  void enable(bool en) {
    if (en && !enabled) {
      updateTicks();
    }
    enabled = en;
  }

private:
  T m_timeout;
  uint32_t ticks;
  bool enabled;

  inline void updateTicks() { ticks = HAL_GetTick(); }
};

/*!
 * @}
 */

#endif // AESFLASHWRITER_H
