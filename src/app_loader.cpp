#include <cstring>

#include <hw_includes.h>

#include "CRC32_platform.h"

#include "Debug.h"
#include "app_settings.h"
#include "applicationsettingsbase.h"
#include "iflashwriter.h"

#include "app_loader.h"

// From linker
extern const uint32_t _sidata;     ///< data start (FLASH)
extern const uint32_t _data_start; ///< data start RAM
extern const uint32_t _data_end;   ///< data end RAM
extern const uint32_t _estack;     ///< last valid ram WORD

struct vectors_table_header {
  uint32_t sp_init_state;
  uint32_t reset_vector;
};

#ifdef NDEBUG
#define HEADER_ASSERT(expr)                                                    \
  {                                                                            \
    if (!(expr))                                                               \
      return false;                                                            \
  }
#else
#define HEADER_ASSERT(expr)                                                    \
  {                                                                            \
    if (!(expr)) {                                                             \
      DEBUG_MSG("APP HEADER ASSERT FAILED: \"" #expr "\"");                    \
      return false;                                                            \
    }                                                                          \
  }
#endif

static const uint8_t *align(const uint8_t *ptr, const size_t alignment) {
  const auto over = (size_t)ptr % alignment;
  return over ? (uint8_t *)(((size_t)ptr / alignment + 1) * alignment) : ptr;
}

const App_loader *App_loader::getApplication() {
  auto app_end = &_sidata + (&_data_end - &_data_start);
  return reinterpret_cast<const App_loader *const>(
      align(reinterpret_cast<const uint8_t *>(app_end), FLASH_PAGE_SIZE));
}

bool App_loader::verifyHeaderFields() const {
  HEADER_ASSERT(app_magick == APP_MAGICK)
  HEADER_ASSERT(app_image_start % sizeof(uint32_t) == 0)
  HEADER_ASSERT(app_image_end % sizeof(uint32_t) == 0)
  HEADER_ASSERT(app_image_end > app_image_start)

  HEADER_ASSERT(app_vectors_pos >= app_image_start)
  HEADER_ASSERT(app_vectors_pos < app_image_end)

  const vectors_table_header *vectors =
      reinterpret_cast<vectors_table_header *>(app_vectors_pos);
  const uintptr_t RAM_ORIGIN = reinterpret_cast<const uintptr_t>(&_sidata);
  const uintptr_t RAM_END = reinterpret_cast<const uintptr_t>(&_estack);

  HEADER_ASSERT(vectors->sp_init_state > RAM_ORIGIN)
  HEADER_ASSERT(vectors->sp_init_state <= RAM_END)

  HEADER_ASSERT(vectors->reset_vector >= app_image_start)
  HEADER_ASSERT(vectors->reset_vector < app_image_end)

  return true;
}

bool App_loader::verify() const {
  const auto calc_crc32 = crc();
  HEADER_ASSERT(calc_crc32 != 0)
  HEADER_ASSERT(calc_crc32 == app_crc32)

  return true;
}

uint32_t App_loader::crc() const {
  if (!verifyHeaderFields())
    return 0; // error, header incorrect

  return CRC32_platform().accamulate(
      reinterpret_cast<uint8_t *>(app_image_start),
      app_image_end - app_image_start);
}

void App_loader::start() const {
  __disable_irq();

  const vectors_table_header *vectors =
      reinterpret_cast<vectors_table_header *>(app_vectors_pos);

  asm volatile("mov     sp, %0" : : "r"(vectors->sp_init_state) :);
  asm volatile("ldr     r12, =0xFFFA");
  asm volatile("bx      %0" : : "r"(vectors->reset_vector) :);

  while (1)
    ;
}

uint32_t App_loader::get_serial() const {
  SharedStorageManager<appSBase::ApplicationSettingsBase<>, flash_read_polcy>
      app_settings_manager(app_settings);
  return appSBase::getSerial(app_settings_manager.load());
}

uint32_t App_loader::size() const {
  return (verifyHeaderFields()) ? app_image_end - app_image_start : 0;
}

void App_loader::configureWriter(IFlashWriter &writer) const {
  writer.setBaseAddress(reinterpret_cast<uint32_t>(this));
}

bool App_loader::isMemoryAvalable(size_t size) const {
  return IS_FLASH_PROGRAM_ADDRESS(reinterpret_cast<uint32_t>(this) + size +
                                  FLASH_PAGE_SIZE - 1);
}

void *App_loader::getSettingsPtr() const {
  return const_cast<void *>(app_settings);
}

uint32_t App_loader::getAppSettingsSize() const {
  SharedStorageManager<appSBase::ApplicationSettingsBase<>, flash_read_polcy>
      app_settings_manager(app_settings);
  return appSBase::getSize(app_settings_manager.load());
}

bool App_loader::getLSSFastScanEnabled() const {
#if CANOPEN_SUPPORT_LOAD_NETWORK_CFG
  SharedStorageManager<appSBase::ApplicationSettingsBase<>, flash_read_polcy>
      app_settings_manager(app_settings);
  return !!appSBase::getLssFastScanEnabled(app_settings_manager.load());
#else
  return true;
#endif
}

LinkInfo App_loader::getStoredLinkInfo() const {
  auto res = LinkInfo();

#if CANOPEN_SUPPORT_LOAD_NETWORK_CFG
  SharedStorageManager<appSBase::ApplicationSettingsBase<>, flash_read_polcy>
      app_settings_manager(app_settings);
  auto settings = app_settings_manager.load();

  res.node_ID = appSBase::getLssNodeID(settings);
  res.bitrate = appSBase::getLssSpeedCode(settings);
  res.update_crc();
#endif

  return res;
}
