/*!
 * \file
 * \brief Implementation of AES stream decryptor
 */

#ifndef AESSTREAMCRYPTER_H
#define AESSTREAMCRYPTER_H

#include <algorithm>
#include <cstring>

#include <mbedtls/aes.h>

#include "iflashwriter.h"

//! \defgroup Crypto Criptographic finctions

/*! \ingroup Crypto
 * @{
 */

//! \brief AES block size in bytes
#define AES_BLOCKSZ (128 / 8)

//! \brief AES CBC IV size
#define IV_SIZE 16

/*!
 * \ingroup Crypto
 * \brief Template class for encrypt AES stream
 *
 * \param keysize The size of data passed. Valid options are:
 *                 <ul><li>16 bytes</li>
 *                 <li>24 bytes</li>
 *                 <li>32 bytes</li></ul>
 */
template <size_t keysize> class AESStreamDeCrypter {
public:
  /*!
   * \brief Constructor
   * \param writer IFlashWriter to perform writes of encrypted data
   */
  AESStreamDeCrypter(IFlashWriter &writer) : writer(writer), overflow_cnt(0) {}

  ~AESStreamDeCrypter() { mbedtls_aes_free(&aes_ctx); }

  /*!
   * \brief Reset decriptr and prepare it to new work
   * \param key AES decription key, bytearray size controled by template
   * argument \param iv AES CBC initialisation vector. Bytearray of size IV_SIZE
   */
  void reset(const uint8_t key[keysize], const uint8_t iv[IV_SIZE]) {
    overflow_cnt = 0;
    mbedtls_aes_init(&aes_ctx);
    mbedtls_aes_setkey_dec(&aes_ctx, key, keysize * 8);
    memcpy(this->iv, iv, IV_SIZE);
  }

  /*!
   * \brief Write encrypted data
   *
   * Accepts encrypted by AES_CBC algorythm of any length and performs
   * encription resupt will be writen by writer delegate
   *
   * \param globalOffset Offset off first byte of data chank
   * \param data Encrypted data array fragment
   * \param size size of data fragment
   * \return On success return FlashStatus::OK or error returned by writer
   * delegate
   */
  FlashStatus write(uint32_t globalOffset, const uint8_t *data,
                    const size_t size) {
    size_t offset = write_prev_overflow(globalOffset, data, size);

    for (; offset + AES_BLOCKSZ < size;
         offset += AES_BLOCKSZ, globalOffset += AES_BLOCKSZ) {
      auto res = decrypt_block(globalOffset, &data[offset]);
      if (res != FlashStatus::OK)
        return res;
    }

    if (offset < size) {
      save_rest_data_fragment(&data[offset], size - offset);
    }
    return FlashStatus::OK;
  }

  /*!
   * \brief Flush rest of data in buffer
   *
   * AES performs decription on fixed size data blocks. If encrypted stream size
   * is not aliquot of AES block size rest data supplemented by zero end
   * encrypt by this method
   * \return \return On success return FlashStatus::OK or error returned by
   * writer delegate
   */
  FlashStatus flush() {
    memset(&overflow_buf[overflow_cnt], 0, AES_BLOCKSZ - overflow_cnt);
    auto res = decrypt_block(overflow_buf);
    overflow_cnt = 0;
    return res;
  }

  /*!
   * \brief Return reference to writer delegate
   */
  IFlashWriter &getWriter() const { return writer; }

private:
  FlashStatus decrypt_block(const uint32_t globalOffset,
                            const uint8_t chank[AES_BLOCKSZ]) {
    uint8_t buf[AES_BLOCKSZ];

    mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_DECRYPT, AES_BLOCKSZ, iv, chank,
                          buf);
    auto res = writer.write(globalOffset, buf, AES_BLOCKSZ);
    LastWritePointer = globalOffset + AES_BLOCKSZ;
    return res;
  }

  FlashStatus decrypt_block(const uint8_t chank[AES_BLOCKSZ]) {
    uint8_t buf[AES_BLOCKSZ];

    mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_DECRYPT, AES_BLOCKSZ, iv, chank,
                          buf);
    auto res = writer.write(LastWritePointer, buf, AES_BLOCKSZ);
    LastWritePointer += AES_BLOCKSZ;
    return res;
  }

  void save_rest_data_fragment(const uint8_t *data, const size_t size) {
    memcpy(overflow_buf, data, size);
    overflow_cnt = size;
  }

  size_t write_prev_overflow(uint32_t &globalOffset, const uint8_t *data,
                             size_t size) {
    if (overflow_cnt != 0) {
      auto readSize = std::min<size_t>(AES_BLOCKSZ - overflow_cnt, size);

      memcpy(&overflow_buf[overflow_cnt], data, readSize);
      overflow_cnt += readSize;
      globalOffset += readSize;

      if (overflow_cnt == AES_BLOCKSZ) {
        decrypt_block(globalOffset - sizeof(overflow_buf), overflow_buf);
        overflow_cnt = 0;
      }

      return readSize;
    }
    return 0;
  }

  IFlashWriter &writer;
  mbedtls_aes_context aes_ctx;
  uint8_t iv[AES_BLOCKSZ];
  uint8_t overflow_buf[AES_BLOCKSZ];
  uint8_t overflow_cnt;
  uint32_t LastWritePointer;
};

/*!
 * @}
 */

#endif // AESSTREAMCRYPTER_H
