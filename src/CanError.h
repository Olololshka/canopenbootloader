#ifndef _CAN_ERROR_H_
#define _CAN_ERROR_H_

#include <stdint.h>

#include "CANopen.h"

#include "iflashwriter.h"

/** \ingroup OD
 * \brief Functions to interface error log (0x1003)
 */

//! \brief Add new error to error log
//! \param status - status code of flash module
void SetFlashError(FlashStatus status);

//! \brief Clear entire error log
void ClearErrorLog();

CO_SDO_abortCode_t processErrorLog(CO_ODF_arg_t *ODF_arg);

#endif /* _CAN_ERROR_H_ */
