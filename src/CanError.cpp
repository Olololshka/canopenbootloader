#include <algorithm>
#include <cstring>

#include "CanError.h"

#define TEST_ERROR_VALUE 0xff1300fe

/*!
 * \brief Class to translate Flash status to Can error log entry
 */
class CanError {
public:
  /*!
   * \brief Constructor
   * \param flashStatus - flasher error code
   */
  constexpr CanError(FlashStatus flashStatus)
      : value(flash2canerror[static_cast<uint32_t>(flashStatus)]) {}

  /*!
   * \brief Convert class instance to uint32_t
   */
  operator uint32_t() const { return value; }

private:
  uint32_t value;

  enum ErrorCategorie {
    NO_ERROR = 0x0000,
    GENERIC_ERROR_GROUP = 0x1000,
    CURRENT_ERROR_GROUP = 0x2000,
    VOLTAGE_ERROR_GROUP = 0x3000,
    TEMPERATURE_ERROR_GROUP = 0x4000,
    DEVICE_HARDWARE_ERROR_GROUP = 0x5000,
    DEVICE_SOFTWARE_ERROR_GROUP = 0x6000,
    ADDITIONAL_MODULES_ERROR_GROUP = 0x7000,
    MONITORING_ERROR_GROUP = 0x8000,
    EXTERNAL_ERROR_GROUP = 0x9000,
    ADDITIONAL_FUNCTIONS_GROUP = 0xF000,
    DEVICE_SPECIFIC_GROUP = 0xFF00
  };

  static const uint32_t flash2canerror[];
};

/*!
 * \brief Flash error code to can error log entry tanslation table
 */
const uint32_t CanError::flash2canerror[] = {
    ErrorCategorie::NO_ERROR,                            /** NO_OPERATION */
    ErrorCategorie::NO_ERROR,                            /** OK */
    ErrorCategorie::DEVICE_HARDWARE_ERROR_GROUP | 0x550, /** LOCKED */
    ErrorCategorie::DEVICE_SOFTWARE_ERROR_GROUP |
        0x300, /** APPLICATION_TOO_LARGE */
    ErrorCategorie::DEVICE_HARDWARE_ERROR_GROUP | 0x580, /** ERASE_FAIL */
    ErrorCategorie::DEVICE_HARDWARE_ERROR_GROUP | 0x590, /** WRITE_FAIL */
    ErrorCategorie::DEVICE_SOFTWARE_ERROR_GROUP | 0x5A0, /** ALIGNMENT_ERROR */
    ErrorCategorie::DEVICE_SOFTWARE_ERROR_GROUP | 0x251, /** SEQUENCE_CORRUPT */
    ErrorCategorie::DEVICE_SOFTWARE_ERROR_GROUP | 0x200, /** INVALID_PROGRAM */
    ErrorCategorie::DEVICE_SOFTWARE_ERROR_GROUP | 0x2FF, /** READY_TO_START */
};

static uint8_t log_entries;

static void ShiftErrorLogDown() {
#if ODL_preDefinedErrorField_arrayLength > 1
  memmove(&OD_preDefinedErrorField[1], &OD_preDefinedErrorField[0],
          (ODL_preDefinedErrorField_arrayLength - 1) * sizeof(uint32_t));
  log_entries = std::min(log_entries + 1, ODL_preDefinedErrorField_arrayLength);
#else
  log_entries = 1;
#endif
}

static void SetCanError(uint32_t error_code) {
  ShiftErrorLogDown();
  OD_preDefinedErrorField[0] = error_code;
}

void SetFlashError(FlashStatus status) { SetCanError(CanError(status)); }

void ClearErrorLog() {
  memset(OD_preDefinedErrorField, 0,
         ODL_preDefinedErrorField_arrayLength * sizeof(uint32_t));
  log_entries = 0;
}

CO_SDO_abortCode_t processErrorLog(CO_ODF_arg_t *ODF_arg) {
  if (ODF_arg->subIndex == 0) {
    if (ODF_arg->reading) {
      ODF_arg->data[0] = log_entries;
    } else {
      if (ODF_arg->dataLength != sizeof(uint8_t)) {
        return CO_SDO_AB_TYPE_MISMATCH;
      }
      switch (ODF_arg->data[0]) {
      case 0:
        ClearErrorLog();
        break;
#ifndef NDEBUG
      case 254:
        SetCanError(TEST_ERROR_VALUE); // Test value
        break;
#endif
      default:
        return CO_SDO_AB_UNSUPPORTED_ACCESS;
      }
    }
  } else {
    if (!ODF_arg->reading) {
      return CO_SDO_AB_READONLY;
    }

    if (ODF_arg->subIndex > log_entries) {
      return CO_SDO_AB_DATA_OD;
    }

    if (ODF_arg->dataLength != sizeof(OD_preDefinedErrorField[0])) {
      return CO_SDO_AB_TYPE_MISMATCH;
    }

    memcpy(ODF_arg->data, &OD_preDefinedErrorField[ODF_arg->subIndex - 1],
           sizeof(OD_preDefinedErrorField[0]));
  }
  return CO_SDO_AB_NONE;
}
