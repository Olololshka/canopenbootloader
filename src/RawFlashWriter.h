#ifndef FLASHWRITER_H
#define FLASHWRITER_H

#include <hw_includes.h>
#include <stdint.h>

#include "iflashwriter.h"

/*!
 * \ingroup Application Flash writer
 * @{
 */

#ifdef STM32L4

// copy-paste from _hal_flash.c for L4
#if defined(STM32L4R5xx) || defined(STM32L4R7xx) || defined(STM32L4R9xx) ||    \
    defined(STM32L4S5xx) || defined(STM32L4S7xx) || defined(STM32L4S9xx)
#define FLASH_NB_DOUBLE_WORDS_IN_ROW 64
#else
#define FLASH_NB_DOUBLE_WORDS_IN_ROW 32
#endif

#define FLASH_ROW_SIZE (FLASH_NB_DOUBLE_WORDS_IN_ROW * sizeof(uint64_t))

#endif

/*!
 * \ingroup Application
 * \brief Flash writer class
 *
 * Perform application write operations with page caching
 */
struct RawFlashWriter : public IFlashWriter {
  /*!
   * \brief Constructor
   * \param baseAddress Simular as parameter of setBaseAddress()
   */
  RawFlashWriter(uint32_t baseAddress = 0)
      : baseAddress(baseAddress), m_lock(false) {}

  uint32_t getBaseAddress() const override;
  FlashStatus setBaseAddress(const uint32_t value) override;
  FlashStatus startTransaction() override;
  FlashStatus write(uint32_t offset, const uint8_t *data, size_t size) override;
  FlashStatus finalise() override;
  void abort() override;

private:
  struct Page_cahce {
    enum class State {
      Idle,
      Clean,
      Duty,
    };

    Page_cahce() : cached_page_base(0), cache_state(State::Idle) {}

    FlashStatus write(uint32_t absolute_address, const uint8_t *data,
                      size_t size);
    void cache_page(uint32_t absolute_address);
    FlashStatus sync();

  private:
    static uint32_t address2page(uint32_t absolute_address);
    size_t page_write(uint32_t offset, const uint8_t *data, size_t size);
    static FlashStatus erease_page(uint32_t page_base);

    template <uint32_t write_type, typename T>
    FlashStatus flash_write_type(T &src_offset, T &destination_addr,
                                 size_t &to_write) {
#if defined(STM32F1) || defined(STM32F3)
      static_assert(write_type >= FLASH_TYPEPROGRAM_HALFWORD, "Mast be 1..3");
      static_assert(write_type <= FLASH_TYPEPROGRAM_DOUBLEWORD, "Mast be 1..3");
#elif defined(STM32L4)
      static_assert(write_type == FLASH_TYPEPROGRAM_DOUBLEWORD,
                    "Mast be 0 for L4 cpu");
#endif

      uint64_t data;
      size_t chank_size;
      auto p = &cached_page_data[src_offset];
      switch (write_type) {
#if defined(STM32F1) || defined(STM32F3)
      case FLASH_TYPEPROGRAM_HALFWORD:
        data = *reinterpret_cast<const uint16_t *>(p);
        chank_size = sizeof(uint16_t);
        break;
      case FLASH_TYPEPROGRAM_WORD:
        data = *reinterpret_cast<const uint32_t *>(p);
        chank_size = sizeof(uint32_t);
        break;
#endif
      case FLASH_TYPEPROGRAM_DOUBLEWORD:
        data = *reinterpret_cast<const uint64_t *>(p);
        chank_size = sizeof(uint64_t);
        break;
      }
      if (HAL_FLASH_Program(write_type, destination_addr, data) != HAL_OK)
        return FlashStatus::WRITE_FAIL;

      src_offset += chank_size;
      destination_addr += chank_size;
      to_write -= chank_size;
      return FlashStatus::OK;
    }

    uint32_t cached_page_base;
    State cache_state;
    uint8_t cached_page_data[FLASH_PAGE_SIZE] __attribute__((aligned(16)));
  };

  void unlock();
  FlashStatus lock();

  Page_cahce cache;
  uint32_t baseAddress;
  bool m_lock;
};

/*! @} */

#endif // FLASHWRITER_H
