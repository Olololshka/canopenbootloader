﻿#ifndef _LSSCLIENT_H_
#define _LSSCLIENT_H_

#include "CANopen.h"
#include "LinkInfoStorage.h"

/// \defgroup LSS LSS protocol support

/*!
 * \ingroup LSS
 * \brief Wrapper for CanOpenNode LSS functions
 */
struct LSSClient {
  /*!
   * \brief Constructor
   */
  LSSClient();

  /*!
   * \brief Constructor
   *
   * \param can_context Pointer to CanOpenNode CO_CANmodule_t struct what
   * Client will be bind
   * \param lss_slave_client Pointer to CanOpenNode CO_LSSslave_t struct what
   * requriad for LSS api
   * \param linkInfo initial state for CAN network
   */
  void configure(CO_CANmodule_t *can_context, CO_LSSslave_t *lss_slave_client,
                 LinkInfo *linkInfo);

  /*!
   * \brief waitAssignment Force wait LSS server assign CAN NODE-ID to this
   * device
   *
   * LSS master mast set node id, then call store configuration (0x17)
   * \param initial_address NODE-ID, what we want to be changed
   * \param timeout if > 0 wait timeout seconds and return, else - infinty wait
   * \return True - assignment passed, else - no LSS communication during
   * timeout
   */
  bool waitAssignment(const uint8_t initial_address, const int32_t timeout);

  /*!
   * \brief Return Assigned link info
   */
  LinkInfo getAssigedLinkInfo() const;

  /*!
   * \brief Set lss service enabled or disabled
   *
   * On disabled state all LSS requests returns errors
   * \param value Enable/Disable LSS service
   */
  void setEnabled(bool value);

  /**
   * @brief preform checking if LSS applaed new configuration and apply it
   * @return true - if network configuration was updated
   */
  bool doCheckReconfigured(const uint8_t &currentAddress,
                           void (*reconfigureCB)(uint8_t));

private:
  CO_CANmodule_t *can_context;
  CO_LSSslave_t *lss_slave_client;
  LinkInfo *linkInfo;

  uint16_t activate_bitrate_delay;
  volatile bool activate_bitrate_triggered;
  bool enabled;

  static bool_t checkBitrate(void *obj, uint16_t bitRate);
  static void applyBitrate(void *obj, uint16_t bitRate);
  static bool_t store_config(void *obj, uint8_t id, uint16_t speed);

  void ApplyBitrate();
};

#endif /* _LSSCLIENT_H_ */
