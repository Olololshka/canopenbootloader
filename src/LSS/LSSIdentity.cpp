#include <climits>

#include "CRC32_platform.h"

#include "CANopen.h"
#include "app_loader.h"

#include "LSSIdentity.h"

static uint32_t CPUIdHash() {
  static const size_t UNIQUE_ID_ADDR_LEN = 96u / CHAR_BIT;

  return CRC32_platform().accamulate(
      reinterpret_cast<const uint8_t *>(UID_BASE), UNIQUE_ID_ADDR_LEN);
}

void LSSIdentity::Save() {
  OD_identityObject.vendorID = vendorID;
  OD_identityObject.productCode = productCode;
  OD_identityObject.revisionNumber = revisionNumber;
  OD_identityObject.serialNumber = serialNumber;
}

void LSSIdentity::Load() {
  vendorID = OD_identityObject.vendorID;
  productCode = OD_identityObject.productCode;
  revisionNumber = OD_identityObject.revisionNumber;
  serialNumber = OD_identityObject.serialNumber;
}

LSSIdentity::LSSIdentity(uint32_t vendorID, uint32_t productCode,
                         uint32_t revisionNumber)
    : vendorID(vendorID), productCode(productCode),
      revisionNumber(revisionNumber), serialNumber{CPUIdHash()} {}

LSSIdentity::LSSIdentity()
    : vendorID(CANOPEN_VENDOR_ID), productCode(CANOPEN_BOOTLOADER_PRODUCT_ID),
      revisionNumber(0), serialNumber{CPUIdHash()} {}

LSSIdentity LSSIdentity::fromApplication(const App_loader *app) {
  auto app_serial = app->get_serial();
  auto serial_is_default = app_serial == 0 || app_serial == 0xFFFFFFFF /* uninitalised flash */;
  return LSSIdentity(app->get_canopen_vendor_id(),
                     app->get_canopen_product_id(), app->get_version(),
                     serial_is_default ? CPUIdHash() : app_serial);
}
