#ifndef LSSIDENTITY_H
#define LSSIDENTITY_H

#include <inttypes.h>

#ifndef CANOPEN_VENDOR_ID
#define CANOPEN_VENDOR_ID 1
#endif

#ifndef CANOPEN_BOOTLOADER_PRODUCT_ID
#define CANOPEN_BOOTLOADER_PRODUCT_ID 0xCB01
#endif

struct App_loader;

/*!
 * \ingroup LSS
 * \brief Provides access to OD_identityObject
 */
class LSSIdentity {
public:
  /*!
   * \brief Constructor
   * \param vendorID Canopen identity Vendor ID (world unique)
   * \param productCode Canopen identity Product code (vendor unique)
   * \param revisionNumber Product version/revision code
   * \param serialNumber Product serial number
   */
  LSSIdentity(uint32_t vendorID, uint32_t productCode, uint32_t revisionNumber,
              uint32_t serialNumber)
      : vendorID(vendorID), productCode(productCode),
        revisionNumber(revisionNumber), serialNumber(serialNumber) {}

  /*!
   * \brief Constructor
   * \param vendorID Canopen identity Vendor ID (world unique)
   * \param productCode Canopen identity Product code (vendor unique)
   * \param revisionNumber Product version/revision code
   *
   * Serial number - crc32(stm32 chip id)
   */
  LSSIdentity(uint32_t vendorID, uint32_t productCode, uint32_t revisionNumber);

  /*!
   * \brief Default constructor
   *
   * Bootloader identity
   */
  LSSIdentity();

  /*!
   * \brief Default copy constructor
   */
  LSSIdentity(const LSSIdentity &) = default;

  /*!
   * \brief Extract Identity information from application
   *
   * If Application invalid or not exists - return default identity
   * If application settings first record contains 0,
   *    then bootloder ID[Serial] = crc32(stm32 chip id)
   *    else serial = from app settings.
   */
  static LSSIdentity fromApplication(const App_loader *app);

  /*!
   * \brief Save LSS Identity to Object Dictionary *[1018]*
   */
  void Save();

  /*!
   * \brief Load LSS Identity from Object Dictionary *[1018]*
   */
  void Load();

  uint32_t vendorID;       ///< Canopen identity Vendor ID (world unique)
  uint32_t productCode;    ///< Canopen identity Product code (vendor unique)
  uint32_t revisionNumber; ///< Product version/revision code
  uint32_t serialNumber;   ///< Product serial number
};

#endif // LSSIDENTITY_H
