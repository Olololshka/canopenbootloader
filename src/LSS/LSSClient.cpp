﻿#include <algorithm>

#include "hw_includes.h"

#include "LinkInfoStorage.h"
#include "PoolingTimer.h"
#include "softreset.h"

#include "DuplicationControl.h"

#include "LSSClient.h"

bool_t LSSClient::checkBitrate(void *obj, uint16_t bitRate) {
  static const uint16_t bitrates[] = {10, 20, 50, 125};
  auto _this = static_cast<LSSClient *>(obj);

  if (!_this->enabled)
    return false;

  const uint16_t *p = std::find(
      bitrates, bitrates + sizeof(bitrates) / sizeof(bitrates[0]), bitRate);
  return (p != bitrates + 4);
}

void LSSClient::applyBitrate(void *obj, uint16_t delay) {
  LSSClient *_this = static_cast<LSSClient *>(obj);

  _this->activate_bitrate_delay = delay;
  _this->activate_bitrate_triggered = true;
}

bool_t LSSClient::store_config(void *obj, uint8_t id, uint16_t speed) {
  auto _this = static_cast<LSSClient *>(obj);
  if (!_this->enabled)
    return false;

  auto thisLinkinfo = _this->linkInfo;

  thisLinkinfo->node_ID = id;
  thisLinkinfo->bitrate = speed;

  thisLinkinfo->update_crc();
  LinkInfoStorage.store(*thisLinkinfo);

  return true;
}

LSSClient::LSSClient()
    : can_context(nullptr), lss_slave_client(nullptr), linkInfo(nullptr),
      activate_bitrate_triggered(false), enabled(false) {}

void LSSClient::configure(CO_CANmodule_t *can_context,
                          CO_LSSslave_t *lss_slave_client, LinkInfo *linkInfo) {
  this->linkInfo = linkInfo;
  this->can_context = can_context;
  this->lss_slave_client = lss_slave_client;

  CO_LSSinit(linkInfo->node_ID, linkInfo->bitrate);

  lss_slave_client->activeNodeID = linkInfo->node_ID;

  CO_LSSslave_initCheckBitRateCallback(lss_slave_client, this, &checkBitrate);
  CO_LSSslave_initActivateBitRateCallback(lss_slave_client, this,
                                          &applyBitrate);
  CO_LSSslave_initCfgStoreCallback(lss_slave_client, this, &store_config);
}

bool LSSClient::waitAssignment(const uint8_t initial_address,
                               const int32_t timeout) {
  uint16_t active_bitrate = linkInfo->bitrate;
  uint8_t activeNodeId = initial_address;

  PoolingTimer<int32_t, 1000> timeoutTimer(timeout);
  timeoutTimer.enable(true);

  while (1) {
    duplication.pool();

    if (activate_bitrate_triggered) {
      ApplyBitrate();
    }

    CO_LSSslave_process(lss_slave_client, active_bitrate, activeNodeId,
                        &active_bitrate, &activeNodeId);

    if (lss_slave_client->lssState == CO_LSS_STATE_CONFIGURATION) {
      timeoutTimer.enable(false); // stop autoboot timer
    }

    if (linkInfo->node_ID != initial_address &&
        lss_slave_client->lssState == CO_LSS_STATE_WAITING) {
      return true;
    }

    if (timeoutTimer.check()) {
      return false;
    }

    // sleep
    __WFI();
  }
}

LinkInfo LSSClient::getAssigedLinkInfo() const { return LinkInfo(*linkInfo); }

void LSSClient::setEnabled(bool value) { enabled = value; }

void LSSClient::ApplyBitrate() {
  HAL_Delay(activate_bitrate_delay);
  if (CO_ERROR_NO != CO_CANmodule_setBitrate(can_context, linkInfo->bitrate)) {
    linkInfo->resetDefaults();
    LinkInfoStorage.store(*linkInfo);
    SoftReset();
  }
  activate_bitrate_triggered = false;
}

bool LSSClient::doCheckReconfigured(const uint8_t &currentAddress,
                                    void (*reconfigureCB)(uint8_t)) {
  bool result = false;

  if (!enabled) {
    return false;
  }

  if (activate_bitrate_triggered) {
    ApplyBitrate();
    result = true;
  }

  if (linkInfo->node_ID != currentAddress) {
    __disable_irq();
    reconfigureCB(linkInfo->node_ID);
    __enable_irq();

    result = true;
  }

  return result;
}
