#ifndef _APP_SETTINGS_H_
#define _APP_SETTINGS_H_

#include "Shared_storage.h"

/*!
 * \ingroup Application
 * \brief Policy to read MCU flash by memcpy()
 */
struct flash_read_polcy : public MemcpyStoragePolcy {
  /*!
   * \brief Constructor
   *
   * Create new object by taget address in flash
   * \param flashAddr Address in flash to read from
   */
  flash_read_polcy(volatile void *flashAddr) : MemcpyStoragePolcy(flashAddr) {}

  /*!
   * \brief Disable store operation for CPU flash
   * \param data unused
   * \param size unused
   * \return always false
   */
  bool store(const void *data, size_t size) const { return false; }
};

#endif /* _APP_SETTINGS_H_ */
