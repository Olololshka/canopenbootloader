#ifndef APPLICATIONSETTINGSBASE_H
#define APPLICATIONSETTINGSBASE_H

#include <cstdint>
#include <cstdlib>
#include <tuple>

namespace appSBase {

/// \brief Application settings base tuple
/// в std::tuple все переменные лежат в памяти в обратном порядке
/// поэтому клиентские поля будут нумероваться с 0, а полседнее поле будет
/// первым в памяти, тоесть serial

template <typename... Targs>
using ApplicationSettingsBase =
    std::tuple<Targs...,
               uint32_t, // settings size
               uint32_t, // settings version
               uint32_t  // serial (если 0 - не используется)
#if CANOPEN_SUPPORT_LOAD_NETWORK_CFG
               ,
               uint32_t, // LSS fast scan Enabled
               uint8_t,  // lss NodeID
               uint16_t  // lss speed code
#endif
               >;

#define def_get_settings_param(name, offset)                                   \
  template <typename T>                                                        \
  auto get##name(T &settings_tuple)                                            \
      ->decltype(std::get<std::tuple_size<T>::value - offset - 1>(             \
          settings_tuple)) & {                                                 \
    return std::get<std::tuple_size<T>::value - offset - 1>(settings_tuple);   \
  }                                                                            \
  template <typename T>                                                        \
  auto get##name(const T &settings_tuple)                                      \
      ->decltype(std::get<std::tuple_size<T>::value - offset - 1>(             \
          settings_tuple)) & {                                                 \
    return std::get<std::tuple_size<T>::value - offset - 1>(settings_tuple);   \
  }

#if CANOPEN_SUPPORT_LOAD_NETWORK_CFG
def_get_settings_param(LssSpeedCode, 0);
def_get_settings_param(LssNodeID, 1);
def_get_settings_param(LssFastScanEnabled, 2);
def_get_settings_param(Serial, 3);
def_get_settings_param(Version, 4);
def_get_settings_param(Size, 5);
#else
def_get_settings_param(Serial, 0);
def_get_settings_param(Version, 1);
def_get_settings_param(Size, 2);
#endif

} // namespace appSBase

#endif // APPLICATIONSETTINGSBASE_H
