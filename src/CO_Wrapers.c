#include <inttypes.h>
#include <string.h>

/*! \defgroup Utils CanOpenNode fuctrion wrapers
 * @{
 */

/*!
 * \brief Wraper for CanOpenNode CO_memcpy() to libc **memcpy()**
 */
void CO_memcpy(uint8_t dest[], const uint8_t src[], const uint16_t size) {
  memcpy(dest, src, size);
}

/*!
 * \brief Wraper for CanOpenNode CO_memset() to libc **memset()**
 */
void CO_memset(uint8_t dest[], uint8_t c, const uint16_t size) {
  memset(dest, c, size);
}

/*! @} */
