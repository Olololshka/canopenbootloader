![alt text][logo]
[logo]: logo.png

# CANopenBootloader

CANopen bootloder for STM32-family MCU. Based on STM32Cube and CanOpenNode libraries

### Features
* NMT client: Supports software node reset and communication reset.
* LSS Client: Supports Node-ID and bus bit rate accepting from network master.
* SDO server: Flash MCU application firmware using bootloader-specific Object Dictionary.
* Start application firmware by writing specific Object Dictionary cell.
* Check application integrity before start.
* User friendly build environment using Cmake.
* Easy way to create shared storage of data accessed for bootloader and application.
* Tested on STM32F373CC and STM32L433CC

### How to use?
This package provides the kit to build your own application what can be flashed
and starts by this bootloader. An empty example of application you can download
from this repository https://bitbucket.org/Olololshka/canopenbootloader.git

#### Requirements
* Build dependencies: cmake, git, python2, python3, STM32Cube, arm-none-eabi-*
toolchain with C++14 support, native linux gcc compiler, mono
* Optional: doxygen

#### 3dr party libraries
* STM32Cube (F3 v1.11.0), (L4 v1.14.0)
* CANopenNode iurly's fork (https://github.com/iurly/CANopenNode)
* libedssharp gotocoffee1's fork (https://github.com/gotocoffee1/libedssharp)
* Mbedtls (https://tls.mbed.org/)

### Standard usage scenario
1. Download sources
2. Setup all necessary packages
3. Configure cmake project
4. Build and install bootloader somewhere
5. Download application template
6. Configure application to use previously installed bootloader

## Application limitations
#### Common requirements
The Application, what can be started by this booloader mast meet several requirements:
1. Mast be linked using specific linker script provided by bootloader package
2. Application mast remap it's interrupt vectors to RAM, or place it in flash aligned by VTOR requirements
3. Application header must be correctly filled.

#### Using shared storage
1. Application mast have specific ram section .shared_storage with address RAM_START + SIZEOF(ISR_VECTOR_TABLE) to access to NODE-ID and network bit rate provided by bootloader
2. Non-NULL pointer in application header, what will be used to get device serial number by bootloader
3. Application must use library libSharedStorage to store it's settings, inheriated of appSBase::ApplicationSettingsBase

## Building bootloader

#### Configuration
* Clone the repository, step into it and initialize it's sub-modules

        git clone https://bitbucket.org/Olololshka/canopenbootloader.git
        cd CANopenBootoader
        git submodule update --init

* Create sub dirrectory for building, step into it and run cmake generator, provide path to STM32Cube by -D option.

        mkdir build && cd build
        cmake .. -DSTM32Cube_DIR=<path to directory, what contains STM32Cube> -DCMAKE_BUILD_TYPE=Release

* Configure bootloader by cmake-gui

        cmake-gui .

* In displayed window check flags *Grouped* and *Advanced*
        Parameters description
        - APP_MAGICK - Application header magic number (may be any 32-bit number)
        - STM32_CHIP - Target STM32 MCU
        - CLOCK_F_CPU - Target chip clock
        - CLOCK_F_XTAL - Target MCU HSE resonator clock
        - CAN_ENABLE_PORT - GPIO port to control CAN bus phisical connection
        - CAN_ENABLE_PIN - GPIO pin to control CAN bus phisical connection (-1 - do not use)  
        - CAN_ENABLE_PORT_INVERT - CAN bus conntct active level (1 or 0)
        - RESERV_ENABLE - Enable [reservation subsystem](@ref reservation)
        - RESERV_DETECT_PORT - GPIO port to reservation detection connected to
        - RESERV_DETECT_PIN - GPIO pin to reservation detection connected to
        - RESERV_DETECT_ACTIVE_LVL - Reserv kit active lvl (1 or 0)
        - RESERV_UART_PORT - UART port number to reservation data exchange
        - CANOPEN_VENDOR_ID - World-unique canopen vendor id
        - CANOPEN_BOOTLOADER_PRODUCT_ID - Bootolader product ID (not application)
        - APP_USE_ENCRYPTION_METHOD - application load-time encryption method
        0 - no encryption, 1 - AES_CBC128 with SHA224 key
        - [CANOPEN_SUPPORT_LOAD_NETWORK_CFG](@ref loadNwCfg) - Enable support load network config from app settings
        - [APP_AUTOBOOT_DELAY_SEC](@ref autoboot) - Autoboot timeout in seconds (-1 - no autoboot)
        - BUILD_USE_LTO - use LTO at linking bootloader **NOT WORKING YET**
        - APP_FLASH_PROTECT - Use mcu self flash protection
        - CMAKE_INSTALL_PREFIX - Directory to install bootloader

**BEWARE TO EDIT OTHER PARAMETERS**

Now run to install bootloader files
```#!bash
make && make install
```

### Installed tree

```
<install root>
├── include
│   └── CANopenBootloader - Headers of common application and bootloader modules
│       ├── applicationsettingsbase.h
│       ├── crc_calculator
│       │   └── CRC32_platform.h
│       ├── LinkInfoStorage
│       │   └── LinkInfoStorage.h
│       └── Shared_storage
│           └── Shared_storage.h
├── lib
│   └── CANopenBootloader
│       ├── bin - Bootloader binaries
│       │   ├── bootloader - ELF with debugging symbols
│       ├── EDSSharp - Object Dictionary generator
│       │   ├── libEDSsharp.dll
│       │   └── OD_src_generator.exe
│       ├── ld
│       │   ├── app_link.ld.in - application linker script template
│       │   └── app_origin.conf - File contains  base address to load application to
│       ├── libcrc_calculator.a - library provides access to hardware crc32 calculator
│       ├── libLinkInfoStorage.a - library provide shared link info  storage
│       └── libShared_storage.a  - library provides Flash settings storage
└── share
    └── CANopenBootloader
        ├── cmake - cmake hapler scripts and modules
        │   ├── bootloader.cmake
        │   ├── bootloader-release.cmake
        │   ├── CANopenBootloaderConfig.cmake - Main cmake module to access from application
        │   ├── crc_calculator.cmake
        │   ├── crc_calculator-release.cmake
        │   ├── LinkInfoStorage.cmake
        │   ├── LinkInfoStorage-release.cmake
        │   ├── Modules - Cmake modules
        │   │   ├── ApplicationHalpers.cmake - Application helper functions
        │   │   ├── CreateODSources.cmake - Cmake interface to generate Object Dictionar sources
        │   │   ├── Doxygen.cmake - Generate documentation module
        │   │   ├── FindMono.cmake
        │   │   ├── FindSTM32Cube.cmake
        │   │   ├── gcc_stm32.cmake
        │   │   ├── gcc_stm32f1.cmake
        │   │   ├── gcc_stm32l1.cmake
        │   │   ├── HybridTargets.cmake - Module to create hybrid  firmware
        │   │   ├── stm32_linker.cmake
        │   │   └── stm32_linker_F4.cmake
        │   ├── Shared_storage.cmake
        │   └── Shared_storage-release.cmake
        └── scripts - cmake-related scripts
            ├── git_version_updater.sh
            └── header_patcher.py
```

## Application header structure

Offset | Name | Type | Description
------ | ---- | ---- |------------
0x00 | app_magic | uint32_t | Application magic number
0x04 | app_image_start | uint32_t  | Address of start of application image, not header
0x08 | app_image_end | uint32_t  | Address of end application image, .settings not included
0x0c | app_canopen_vendor_id | uint32_t | Application vendor ID
0x10 | app_canopen_product_id | uint32_t | Application product ID
0x14 | app_version | uint32_t | Application version
0x18 | app_crc32 | uint32_t | Application checksum Ethernet algorithm, excluding settings and header sections
0x1c | app_settings | void* | pointer to application .settings section
0x20 | app_vectors_pos | uint32_t | address of FLASH-stored ISR vector table

Fields *app_magic, app_image_start, app_image_end, app_crc32, app_settings, app_vectors_pos*
provides then linking the application by linker script. Others must be privided by user

## Object Dictionary
Object | Type | Description
------ | ---- | -----------
0x1000 | Uint32 | Device Type (const: 0x00000001)
0x1001 | Unit8 | Error register
0x1003 | - | Pre-defined error field
0x1003sub0 | Uint8 | Number of errors
0x1003sub1 | Uint32 | Standard error field
0x1008 | String | Manufacturer device name (const: boot)
0x1009 | String | Manufacturer hardware version (const: 1.00)
0x100A | String | Manufacturer software version (const: 2.1)
0x1018 | - | Identity Object
0x1018sub0 | Uint8 | Number of entries
0x1018sub1 | Uint32 | Vendor ID (const: 0x004f038C)
0x1018sub2 | Uint32 | Product code (const: 0xCAB1)
0x1018sub3 | Uint32 | Revision number (const: 0)
0x1018sub4 | Uint32 | SerialNumber (const: 1)
0x1029 | Uint32 | Error behavior
0x1029sub0 | Uint8 | max sub-index
0x1029sub1 | Uint8 | Communication
0x1029sub2 | Uint8 | Generic error
0x1029sub3 | Uint8 | Device profile error
0x1029sub4 | Uint8 | Manufacturer specific error
0x1200 | - | SDO server parameter
0x1200sub0 | Uint8 | max sub-index
0x1200sub1 | Uint32 | COB-ID client to server (const: $NODEID+0x600)
0x1200sub2 | Uint32 | COB-ID server to client (const: $NODEID+0x580)
0x1F50 | - | Program Data
0x1F50sub0 | Uint8 | Number of entries
0x1F50sub1 | DOMAIN | Program number 1
0x1F51 | - | Program control
0x1F51sub0 | Uint8 | Number of entries
0x1F51sub1 | Uint8 | Program control 1
0x1F52 | - | Reservation control
0x1F52sub0 | Uint8 | Number of entries
0x1F52sub1 | Uint8 | Start reserv 1
0x1F55 | - | Application image encription method
0x1F55sub0 | Uint8 | Number of entries
0x1F55sub1 | Uint32 | Application 1 encription method
0x1F56 | - | Identifies the application
0x1F56sub0 | Uint8 | Number of entries
0x1F56sub1 | Uint32 | Identifies the application (CRC32) 1
0x1F5A | - | Application settings data
0x1F5Asub0 | Uint8 | Number of entries
0x1F5Asub1 | DOMAIN | Application settings data 1

## Object Dictionary explanation

### 0x1000 - Device Type
Bitfield, mondatory are 4 LSB bits
* 1st - Device has digital input
* 2nd - Device has digital output
* 3rd - Device has analog input
* 4th - Device has analog output

Mostly applicatable to bootloader value 0x00000001

### 0x1001 - Error register
Contains 8-bit field with error overview
At lead bit 0 - Generic error mast be implemented

### 0x1003 - Pre-defined error field
Error log, Contains last errors, but in bootloader contains only one record,
Clear error log may be performed by writing 0 to 0x1003sub0, else ABORT
Structure of field
| Bits 24-31 | Bits 16-23 | Bits 0-15 |
| ---------- | ---------- | --------- |
| Manufacturer-specific error code | Error register | Error code |

### Manufacturer descriptions
Elements 0x1008 - 0x100A contains standart text values (compability)

### 0x1018 - Identity Object
Standard field, what contains device id, what uses to perform LSS communication.
If application is valid, this values will be loaded from application header,
else - default values.

### 0x1029 - Error behavior
Standard field. Subfields contains codes of states device must transit
if corresponding error occurred. Bootloader not supported states transitions, so
this values will be ignored. Compatibility issue.

### 0x1200 - SDO server parameter
Record contains two constant values, what contains COB-IDs of SDO server
Only for client information

### 0x1F50 - Program Data
Record, contains only one item 0x1F50sub1. This element is **write only**.
Any binary data writen to this item in fact will be writen to application flash
area. So, to update application user mast write it's binary image directly to
this element.

### 0x1F51 - Program control {#control}
Record, contains only one item 0x1F51sub1. Writing any non-zero value means trying
to start the application. On success control of the all execution transmits to
the application, else bootloader continues running and status of 0x1F57sub0 will
be changed to INVALID_PROGRAM.

### 0x1F52 - Reservation control
Record, contains only one item 0x1F52sub1. Writing any non-zero value means **halts** current
working kit. Reserved kit mast starts after status messages ressive timeout.

### 0x1F55 - Identifies the application
Application image encryption method.
Before upload application image to 0x1F50sub1 userm mast select image encryption method.

| Value | Method |
| ----- | ------ |
| 0     | No encryption, upload raw binary image |
| 1     | AES128 CBC |

see [image encryption](@ref encryption)

### 0x1F56 - Identifies the application
Record, contains only one item 0x1F56sub1. This value contains current application
CRC32 (real, calculated on request) or 0, if application invalid or not present.
CRC32 can be used to verify writen proggram image.

### 0x1F5A - Application settings data
Binnary image of all application settings. Bootloader didn't know settings structure
except mondatory fields

| Offset | Type     | Description         |
| ------ | -------- | ------------------- |
| 0      | uint32_t | Serial number       |
| 4      | uint32_t | Settings version    |
| 8      | uint32_t | Settings image size |

see appSBase::ApplicationSettingsBase

This index used in firmware update process

## Encrypting application code {#encryption}
Encryption of the application can be used to prevent unauthorized access to the
application during the firmware update on the client-side. The whole application
image is encrypts using any algorithm, except the title (9 32bit words).
Encription key mast be generated from  this header by any hash algorihm.
On writing, the key is restored from the unencrypted header by booloader and the
rest of the data is decrypts by this key. Then the decrypted data writes to
flash memory.

Firmware provider:

- Compile firmware to binary image
- Convert first 36 bytes to encryption key
- Encrypt rest binary data by generated key
- Send encrypted firmware image to customer

Customer:

- Configure device by LSS
- Select encryption method by writing 1 to 0x1F55sub1
- Send encrypted image to 0x1F50sub1

Bootloader:

- Select decription method depending on the 0x1F55sub1б if selcet encryption (1) do *following*
- Get first 36 bytes of encrepted firmware image
- Convert it to decryption key
- Decrypt remaining data by generated key
- Write decrypted application image to application flash area
- *or, directly write image to flash*
- Verify application can be executed

The default encryption is AES128 CBC with a key generated by the SHA224 algorithm
CBC iv -> first 16 bytes of application header, but magick (header[4:20])
See src/SHA2ToKey.h and src/AESStreamDeCrypter.h for details.

## Save and restore application settings
User can backup application settings and restore it iing index 0x1F5Asub1
It's possible backup settings before upload new application image and write it backup
after success update. BUT application mast convert existing settings to new format if
it changes by itself. Check field `settings.version` before load settings.

## Error codes
Bootloader stores last error code in 0x1003sub1 if any present, user can clear
last error by writing 0 to 0x1003sub0, any other values forbidden
Posible error codes described in src/CanError.cpp

## Flashing bootloader to target MCU

1. Ensure what openocd connected to the target MCU over SWD or JTAG (gdb port 2331)
2. Build bootloader
3. From build dirrectory execute gdb flash script

        arm-none-eabi-gdb -x <booloader_root_dir>/flash_bootloader.gdb


## Code protection
When launching for the first time with APP_FLASH_PROTECT=TRUE, the bitloader sets the
flash memory protection according to https://stackoverflow.com/a/41106997/8065921
see \a flashProtect() in src/BoardInit.cpp for details.
To remove it, you need to connect a debugger under the reset

```
(gdb) monitor reset_config srst_only srst_nogate # enable connect under reset
# hold reset to 0 manualy
(gdb) monitor reset halt
# release reset manualy
(gdb) stm32f<family code>x unlock 0
```

A message appears prompting you to reset the processor. The most reliable way
to turn off and turn on the power, Reset for some reason does not work.
Next, you should clean the old firmware

```
(gdb) flash erase_sector 0 0 <last block number>
```

Now you can flash new firmware usual way.

_For some reasons modern openocd protection reseting not working. Use openocd-esp32 instead._

## Autoboot {#autoboot}
Bootloader provides optional application autoboot, if no LSS put to CONFIGURATION state
messages was sent during timeout.
Limitations:

- Application mast be valid
- Autoboot disabled, if bootloader loads `node_ID != CO_LSS_NODE_ID_ASSIGNMENT`
into `linkInfo`, This means the application has been configured and deliberately caused a reboot.
- Disable autoboot required if uses CiA 443 profile

## Load network settings from app's Settings {#loadNwCfg}
Optionaly (CANOPEN_SUPPORT_LOAD_NETWORK_CFG) bootloader can load CANOpen network settings
from appSBase::ApplicationSettingsBase's optional fields. It has special space to store
power-on network settings. If this feature enabled bootloader will use this settings on boot up 
instead of unconfigured state.

## Reservation subsystem {#reservation}
Bootloader provides reservation subsystem over DuplicationControl library.
1. I one phisical device two identical CPU presents. Master Kit and Reserv Kit.
2. Master kit connected to Reserv by UART interface TX (M) -> RX (R), no back connection
3. Two CPUs runns identical code (this).
4. Firmware detects Reserv kit by selectable GPIO PIN (RESERV_DETECT_PORT/
    RESERV_DETECT_PIN/RESERV_DETECT_ACTIVE_LVL).
5. Master kit starts immediately, Reserv - waits condition before start
6. Master kit mast hold Reserv kit in special state by sending periodical repors of it's status
7. Slave kit starts automaticaly, if it's not ressived any reports from Master kit for period of time
    If slave kit finaly starts it mast phisiacaly break connecton of master kit to CAN bus.
8. Master kit informs Slave kit for BUS-level events, such as:
    - Master's CANOpen state (PRE-OP, WORKING...)
    - CANOpen network config changed (node_ID or speed)
    - Master work mode (bootloader/aplication)
    - Application OD was changed and saved.
9. Bootloader provides configured libDuplicationControl to Application to ensure compatibility.

### Reservation Master to Reserv messages
1. Status report: PMKR
| Offset | Type     | Description                    |
| ------ | -------- | ------------------------------ |
| 0      | uint8_t  | MAGICK (0x84)                  |
| 1      | uint8_t  | Code (0)                       |
| 2      | uint8_t  | CAN state code                 |
| 3      | uint8_t  | IsApplication running (1 or 0) |

2. Network config changed: NCC
Transmitts serialised object of type `LinkInfo`
| Offset     | Type      | Description                    |
| ---------- | --------- | ------------------------------ |
| 0          | uint8_t   | MAGICK (0x84)                  |
| 1          | uint8_t   | Code (1)                       |
| 2          | uint8_t   | Serialised data size           |
| 3 - 3+size | uint8_t[] | Serialised data                |


## SWD debug messages
According to https://blog.japaric.io/itm/
- Connect SWO pin to debugger
- In source code replace weak function \a _write() as in src/SWO_debug.c
- Now printf() and std::cout will write to ITM
- From debuger enable messages

        (gdb) monotor tpiu config internal /tmp/itm.fifo uart off 10000000 2000000
        (gdb) monitor itm port 0 1

    * /tmp/itm.fifo - FIFO file to write messages (created before)
    * uart - SWO mode - basic UART
    * off - no formatter
    * 10000000 - cpu frequency
    * 2000000 - SWO frequency

- Compile and execute https://github.com/esynr3z/openocd-stuff/blob/master/tools/itmdump/itmdump.c

        ./itmdump -f /tmp/itm.fifo -d1

    * /tmp/itm.fifo - FIFO file to read
    * -d1 - decode data to text

You can connect itmdump to qtcreator as external tool, more info: https://habr.com/ru/post/440024/

## Tests
Tests [repository](https://github.com/ololoshka2871/canopen-bbb)
Use subdirrectory `tests-bootloader`
Default: can0 and 50000bps, edit common.py to costomise


## Progress:
See [TODO](@ref todo)

## Known issies

* If build for different MCUs make shure what cmake script GET_MACRO_VALUE works correctly

    When you copy configured dirrectory and set new MCU target the macro GET_MACRO_VALUE still use old
    config from original dirrectory. You can temporary rename it and run `cmake .` to reconfigure

* Then debug serial line over SWO enabled, in openocd loading firmware allways fails on F3,
    no idea how to fix instead restart openocd. L4 - ok.
